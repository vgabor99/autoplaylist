plugins {
    id("autoplaylist-app")
    id("com.google.firebase.crashlytics")
    id("com.google.gms.google-services") apply false
}

android {
    defaultConfig {
        applicationId = "com.vargag99.autoplaylist"
        versionCode = 201
        versionName = "2.01"
    }
}

dependencies {
    implementation(project(":domain"))
    implementation(project(":domainimpl"))
    implementation(Deps.Koin.android)
    implementation(Deps.Koin.androidxScope)
    implementation(Deps.Koin.androidxViewmodel)
    implementation(Deps.timber)
    implementation(platform(Deps.Firebase.bom))
    implementation(Deps.Firebase.core)
    implementation(Deps.Firebase.crashlytics)
    implementation(Deps.Androidx.fragmentKtx)
    implementation(Deps.Androidx.constraintlayout)
    implementation(Deps.Lifecycle.extensions)
    implementation(Deps.Lifecycle.viewmodelKtx)
    implementation(Deps.Lifecycle.livedataKtx)
    implementation(Deps.Lifecycle.reactivestreamsKtx)
    implementation(Deps.switcherx)
    implementation(Deps.BindingCollectionAdapter.adapter)
    implementation(Deps.BindingCollectionAdapter.recyclerview)
    implementation(Deps.material)
}

apply(mapOf("plugin" to "com.google.gms.google-services"))
