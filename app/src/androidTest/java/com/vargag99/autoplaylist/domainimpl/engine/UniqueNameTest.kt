package com.vargag99.autoplaylist.domainimpl.engine

import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class UniqueNameTest {
    @Test
    fun testOneRound() {
        val dirs = makeDirs(
            "/sdcard/unique",  // Unique
            "/sdcard/dir/middlenotend",  // The end is unique
            "/sdcard/dir",  // 1 rounds
            "/sdcard/foo/dir",  // 1 rounds
            "/sdcard/bar/dir") // 1 rounds
        makeInitialPlayListNames(dirs)
        makeUniquePlayListNames(dirs)
        assertPlaylists(dirs,
            dir("/sdcard/unique", "@ unique"),
            dir("/sdcard/dir/middlenotend", "@ middlenotend"),
            dir("/sdcard/dir", "@ sdcard/dir"),
            dir("/sdcard/foo/dir", "@ foo/dir"),
            dir("/sdcard/bar/dir", "@ bar/dir"))
    }

    @Test
    fun testTwoRounds() {
        val dirs = makeDirs(
            "/sdcard/unique",  // Unique
            "/sdcard/dir/middlenotend",  // The end is unique
            "/sdcard/dir",  // 1 rounds
            "/sdcard/foo/baz/dir",  // 2 rounds
            "/sdcard/bar/baz/dir") // 2 rounds
        makeInitialPlayListNames(dirs)
        makeUniquePlayListNames(dirs)
        assertPlaylists(dirs,
            dir("/sdcard/unique", "@ unique"),
            dir("/sdcard/dir/middlenotend", "@ middlenotend"),
            dir("/sdcard/dir", "@ sdcard/dir"),
            dir("/sdcard/foo/baz/dir", "@ foo/baz/dir"),
            dir("/sdcard/bar/baz/dir", "@ bar/baz/dir"))
    }

    @Test
    fun testMoreRounds() {
        val dirs = makeDirs(
            "/sdcard/1/2/3/4/5/dir",
            "/sdcard/2/3/4/5/dir",
            "/sdcard/3/4/5/dir",
            "/sdcard/4/5/dir")
        makeInitialPlayListNames(dirs)
        makeUniquePlayListNames(dirs)
        assertPlaylists(dirs,
            dir("/sdcard/1/2/3/4/5/dir", "@ 1/2/3/4/5/dir"),
            dir("/sdcard/2/3/4/5/dir", "@ sdcard/2/3/4/5/dir"),
            dir("/sdcard/3/4/5/dir", "@ sdcard/3/4/5/dir"),
            dir("/sdcard/4/5/dir", "@ sdcard/4/5/dir"))
    }

    @Test
    fun testOneMaxOutSingle() {
        // One dir maxes out but the other still can go on and resolve.
        val dirs = makeDirs(
            "/foo",  // This will max out...
            "/foo/foo") // ...but this is still good to go.
        makeInitialPlayListNames(dirs)
        makeUniquePlayListNames(dirs)
        assertPlaylists(dirs,
            dir("/foo", "@ foo"),
            dir("/foo/foo", "@ foo/foo"))
    }

    @Test
    fun testOneMaxOutDouble() {
        // One dir maxes out but the other still can go on and resolve.
        val dirs = makeDirs(
            "/sdcard/dir",  // This will max out...
            "/sdcard/dir/sdcard/dir") // ...but this is still good to go.
        makeInitialPlayListNames(dirs)
        makeUniquePlayListNames(dirs)
        assertPlaylists(dirs,
            dir("/sdcard/dir", "@ sdcard/dir"),
            dir("/sdcard/dir/sdcard/dir", "@ dir/sdcard/dir"))
    }

    @Test
    fun testTwoMaxOutSingle() {
        // Two dirs max out.
        val dirs = makeDirs(
            "/foo",
            "/foo/foo",
            "/foo/foo/foo")
        makeInitialPlayListNames(dirs)
        makeUniquePlayListNames(dirs)
        assertPlaylists(dirs,
            dir("/foo", "@ foo"),
            dir("/foo/foo", "@ foo/foo"),
            dir("/foo/foo/foo", "@ foo/foo/foo"))
    }

    @Test
    fun testTwoMaxOutDouble() {
        // Two dirs max out.
        val dirs = makeDirs(
            "/sdcard/dir",
            "/sdcard/dir/sdcard/dir",
            "/sdcard/dir/sdcard/dir/sdcard/dir")
        makeInitialPlayListNames(dirs)
        makeUniquePlayListNames(dirs)
        assertPlaylists(dirs,
            dir("/sdcard/dir", "@ sdcard/dir"),
            dir("/sdcard/dir/sdcard/dir", "@ sdcard/dir/sdcard/dir"),
            dir("/sdcard/dir/sdcard/dir/sdcard/dir", "@ dir/sdcard/dir/sdcard/dir"))
    }

    @Test
    fun testThreeMaxOutSingle() {
        // Three dirs max out.
        val dirs = makeDirs(
            "/foo",
            "/foo/foo",
            "/foo/foo/foo",
            "/foo/foo/foo/foo")
        makeInitialPlayListNames(dirs)
        makeUniquePlayListNames(dirs)
        assertPlaylists(dirs,
            dir("/foo", "@ foo"),
            dir("/foo/foo", "@ foo/foo"),
            dir("/foo/foo/foo", "@ foo/foo/foo"),
            dir("/foo/foo/foo/foo", "@ foo/foo/foo/foo"))
    }

    @Test
    fun testThreeMaxOutDouble() {
        // Three dirs max out.
        val dirs = makeDirs(
            "/sdcard/dir",
            "/sdcard/dir/sdcard/dir",
            "/sdcard/dir/sdcard/dir/sdcard/dir",
            "/sdcard/dir/sdcard/dir/sdcard/dir/sdcard/dir")
        makeInitialPlayListNames(dirs)
        makeUniquePlayListNames(dirs)
        assertPlaylists(dirs,
            dir("/sdcard/dir", "@ sdcard/dir"),
            dir("/sdcard/dir/sdcard/dir", "@ sdcard/dir/sdcard/dir"),
            dir("/sdcard/dir/sdcard/dir/sdcard/dir", "@ sdcard/dir/sdcard/dir/sdcard/dir"),
            dir("/sdcard/dir/sdcard/dir/sdcard/dir/sdcard/dir", "@ dir/sdcard/dir/sdcard/dir/sdcard/dir"))
    }

    @Test
    fun testCaseInsensitive() {
        // Conflict resolution should be case- and accent-insensitive.
        // (Accents not tested - the same code runs and is difficult to code accented text.)
        val dirs = makeDirs(
            "/sdcard/foo/abc",
            "/sdcard/bar/ABC",
            "/sdcard/baz/Abc")
        makeInitialPlayListNames(dirs)
        makeUniquePlayListNames(dirs)
        assertPlaylists(dirs,
            dir("/sdcard/foo/abc", "@ foo/abc"),
            dir("/sdcard/bar/ABC", "@ bar/ABC"),
            dir("/sdcard/baz/Abc", "@ baz/Abc"))
    }

    @Test
    fun testRealLifeCaseSensitive() {
        // Maxing out - conflict is due to case insensitivity, we must use postfixes.
        val dirs = makeDirs(
            "/sdcard/Music",
            "/sdcard/music",
            "/sdcard/DCIM",
            "/sdcard/dcim")
        makeInitialPlayListNames(dirs)
        makeUniquePlayListNames(dirs)
        assertPlaylists(dirs,
            dir("/sdcard/Music", "@ Music(1)"),
            dir("/sdcard/music", "@ music(2)"),
            dir("/sdcard/DCIM", "@ DCIM(1)"),
            dir("/sdcard/dcim", "@ dcim(2)"))
    }

    @Test
    fun testMaxOut() {
        // Maxing out - conflict is due to case insensitivity, we must use postfixes.
        val dirs = makeDirs(
            "/sdcard/abc/dir",
            "/sdcard/ABC/dir",
            "/sdcard/Abc/dir")
        makeInitialPlayListNames(dirs)
        makeUniquePlayListNames(dirs)
        assertPlaylists(dirs,
            dir("/sdcard/ABC/dir", "@ dir(1)"),
            dir("/sdcard/Abc/dir", "@ dir(2)"),
            dir("/sdcard/abc/dir", "@ dir(3)"))
    }

    @Test
    fun testMultipleMaxOutInPairs() {
        // Dirs max out in pairs.
        val dirs = makeDirs(
            "/foo/foo",
            "/foo/FOO",
            "/foo/foo/foo",
            "/foo/foo/FOO")
        makeInitialPlayListNames(dirs)
        makeUniquePlayListNames(dirs)
        assertPlaylists(dirs,
            dir("/foo/FOO", "@ FOO(1)"),
            dir("/foo/foo", "@ foo(2)"),
            dir("/foo/foo/FOO", "@ FOO(3)"),
            dir("/foo/foo/foo", "@ foo(4)"))
    }

    @Test
    fun testMaxOutHitsNatural() {
        // Maxing out - conflict is due to case insensitivity, we must use postfixes.
        // The first round hits a "natural" postfix that is actually part of a dir name.
        // So this takes back the postfixed one to normal.
        val dirs = makeDirs(
            "/sdcard/abc/dir",
            "/sdcard/ABC/dir",
            "/sdcard/Abc/dir",
            "/sdcard/dir(2)")
        makeInitialPlayListNames(dirs)
        makeUniquePlayListNames(dirs)
        assertPlaylists(dirs,
            dir("/sdcard/ABC/dir", "@ dir(1)"),
            dir("/sdcard/Abc/dir", "@ sdcard/Abc/dir"),  // After postfix resolution to 'dir(2)', conflicts with normal.
            dir("/sdcard/abc/dir", "@ dir(3)"),
            dir("/sdcard/dir(2)", "@ sdcard/dir(2)")) // Original name that conflicted with postfixed dir(2).
    }

    class Dir(val fn: String, val playListName: String) {
        override fun toString(): String {
            return "$fn->$playListName"
        }

    }

    // Some bits and pieces to ease testing.
    private fun dir(fn: String, playListName: String): Dir {
        return Dir(fn, playListName)
    }

    private fun makeDirs(vararg dirNames: String): Map<String, DirInfo> {
        // Make empty dir map containing the dirNames.
        val map = mutableMapOf<String, DirInfo>()
        for (dirName in dirNames) {
            map[dirName] = DirInfo(dirName)
        }
        return map
    }

    private fun assertPlaylists(actual: Map<String, DirInfo>, vararg expected: Dir) {
        // Assert the playlist names, we are only testing the names here.
        Assert.assertEquals(expected.size.toLong(), actual.size.toLong())
        for (dir in expected) {
            Assert.assertEquals(dir.playListName, actual[dir.fn]?.playListName)
        }
    }
}
