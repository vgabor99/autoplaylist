package com.vargag99.autoplaylist.domainimpl.engine

import android.content.ContentResolver
import android.content.Context
import android.database.MatrixCursor
import android.provider.MediaStore
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.vargag99.autoplaylist.domain.analytics.Analytics
import io.mockk.every
import io.mockk.mockk
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class NullValuesTest {

    @Test
    fun testTrackInfo() {
        // Test that TrackInfo is happy with all kinds of data.
        assertTrackInfo(TrackInfo.from(1, "/foo"), 1, "/foo", "/")
        assertTrackInfo(TrackInfo.from(1, "/dir/foo"), 1, "/dir/foo", "/dir")
        // Here come the weird ones, normally we don't get these.
        assertNull(TrackInfo.from(1, "foo"))
        assertTrackInfo(TrackInfo.from(1, "dir/foo"), 1, "dir/foo", "dir")
        assertNull(TrackInfo.from(1, ""))
        assertNull(TrackInfo.from(1, null))
    }

    private fun assertTrackInfo(actual: TrackInfo?, id: Long, fn: String?, dir: String?) {
        requireNotNull(actual)
        assertEquals(id, actual.id)
        assertEquals(fn, actual.fn)
        assertEquals(dir, actual.dir)
    }

    @Test
    fun testGetTracks() {
        val context = mockk<Context>()
        val contentResolver = mockk<ContentResolver>()
        val analytics = mockk<Analytics>()
        val cursor = MatrixCursor(arrayOf(MediaStore.Audio.Media._ID, MediaStore.Audio.Media.DATA)).apply {
            addRow(arrayOf(1, "/dir1/foo"))
            addRow(arrayOf(2, "/dir1/bar"))
            addRow(arrayOf(3, "/dir2/foo"))
            addRow(arrayOf(4, "/dir2/bar"))
            addRow(arrayOf(5, "absolute/foo"))
            addRow(arrayOf(6, "absolute/bar"))
            addRow(arrayOf(7, "parentLess-foo"))
            addRow(arrayOf(8, "parentLess-bar"))
            addRow(arrayOf<Any?>(9, null))
            addRow(arrayOf<Any?>(10, null))
        }
        every { context.contentResolver } returns contentResolver
        every {
            contentResolver.query(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                any(),
                any(),
                any(),
                any()
            )
        } returns cursor

        val mediaContentHandler = MediaContentHandlerImpl(contentResolver, analytics)
        val tracks = mediaContentHandler.getTracks()
        assertEquals(6, tracks.size)
        val dirs = tracks.groupByDirs()
        assertEquals(3, dirs.size)
    }
}
