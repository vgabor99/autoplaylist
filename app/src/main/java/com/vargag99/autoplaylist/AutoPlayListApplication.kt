package com.vargag99.autoplaylist

import android.app.Application
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.vargag99.autoplaylist.domainimpl.androidServicesModule
import com.vargag99.autoplaylist.domainimpl.domainModule
import com.vargag99.autoplaylist.ui.uiModule
import com.vargag99.autoplaylist.util.CrashlyticsTree
import com.vargag99.autoplaylist.util.TimberKoinLogger
import io.reactivex.plugins.RxJavaPlugins
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import timber.log.Timber

class AutoPlayListApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        Timber.plant(if (BuildConfig.DEBUG) Timber.DebugTree() else CrashlyticsTree())
        if (!BuildConfig.DEBUG) {
            RxJavaPlugins.setErrorHandler { throwable ->
                Timber.e(throwable)
                FirebaseCrashlytics.getInstance().recordException(throwable)
            }
        }
        startKoin {
            logger(TimberKoinLogger(level = if (BuildConfig.DEBUG) Level.DEBUG else Level.ERROR))
            androidContext(this@AutoPlayListApplication)
            modules(androidServicesModule, domainModule, uiModule)
        }
    }
}
