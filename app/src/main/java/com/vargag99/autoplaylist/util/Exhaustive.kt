package com.vargag99.autoplaylist.util

val Any?.exhaustive get() = this as Unit
