package com.vargag99.autoplaylist.ui.folders

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.vargag99.autoplaylist.BR
import com.vargag99.autoplaylist.R
import com.vargag99.autoplaylist.databinding.FoldersActivityBinding
import com.vargag99.autoplaylist.domain.analytics.Analytics
import com.vargag99.autoplaylist.domain.analytics.Properties
import com.vargag99.autoplaylist.domain.analytics.alert
import com.vargag99.autoplaylist.domain.analytics.click
import com.vargag99.autoplaylist.ui.generic.consume
import com.vargag99.autoplaylist.util.exhaustive
import me.tatarka.bindingcollectionadapter2.OnItemBind
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class FoldersActivity : AppCompatActivity() {
    private val analytics: Analytics by inject()
    private val viewModel: FoldersViewModel by viewModel()
    private lateinit var binding: FoldersActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.folders_activity)
        binding.itemBinding = OnItemBind<FoldersItem> { itemBinding, _, item ->
            itemBinding.set(BR.item, when (item) {
                FoldersItem.Select -> R.layout.folders_item_select
                FoldersItem.Default -> R.layout.folders_item_default
                is FoldersItem.Folder -> R.layout.folders_item_folder
                FoldersItem.Add -> R.layout.folders_item_add
            })
                .bindExtra(BR.viewModel, viewModel)
        }
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        viewModel.navigation.consume(this) { target ->
            when (target) {
                FoldersTarget.Back -> onBackPressed()
                FoldersTarget.PickFolder -> pickFolder()
                is FoldersTarget.Error -> showErrorDialog(target)
            }.exhaustive
        }
    }

    override fun onResume() {
        super.onResume()
        analytics.screen(this, "Folders")
    }

    override fun onPause() {
        super.onPause()
        if (isFinishing) overridePendingTransition(R.anim.no_animation, R.anim.exit_to_left)
    }

    private fun pickFolder() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT_TREE).apply {
            flags = Intent.FLAG_GRANT_READ_URI_PERMISSION or
                Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION
        }
        startActivityForResult(intent, PICK_FOLDER_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_FOLDER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val uri = data?.data ?: return
            viewModel.onDirectoryPicked(uri)
        }
    }

    private fun showErrorDialog(target: FoldersTarget.Error) {
        val (title, message) = when (target) {
            is FoldersTarget.Error.UnsupportedFolderError -> Pair(
                getString(R.string.error_dialog_title_bad_folder),
                getString(R.string.error_dialog_message_bad_folder)
            )
            is FoldersTarget.Error.UnsuportedVolumeError -> Pair(
                getString(R.string.error_dialog_title_internal_only),
                getString(R.string.error_dialog_message_internal_only)
            )
            else -> Pair(
                getString(R.string.error_dialog_title),
                getString(R.string.error_dialog_message, target.throwable)
            )
        }
        analytics.alert("Error", Properties.MESSAGE to message)
        MaterialAlertDialogBuilder(this@FoldersActivity)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(android.R.string.ok) { _, _ ->
                analytics.click("OK")
            }
            .create()
            .show()
    }
}

private const val PICK_FOLDER_REQUEST_CODE = 456
