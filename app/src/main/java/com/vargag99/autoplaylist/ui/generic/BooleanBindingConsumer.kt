package com.vargag99.autoplaylist.ui.generic

// Support binding to lambdas: define interfaces
interface BooleanBindingConsumer {
    fun invoke(value: Boolean)
}
