package com.vargag99.autoplaylist.ui.init

sealed class InitTarget {
    class RequestPermission(val permissions: List<String>) : InitTarget()
    object Main : InitTarget()
    class Error(val throwable: Throwable) : InitTarget()
}
