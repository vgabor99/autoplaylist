package com.vargag99.autoplaylist.ui.init

import androidx.core.content.PermissionChecker.PERMISSION_GRANTED
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.vargag99.autoplaylist.domain.analytics.Analytics
import com.vargag99.autoplaylist.domain.analytics.Events
import com.vargag99.autoplaylist.domain.analytics.Properties
import com.vargag99.autoplaylist.domain.analytics.click
import com.vargag99.autoplaylist.domain.engine.Engine
import com.vargag99.autoplaylist.domain.firstrun.FirstRunHandler
import com.vargag99.autoplaylist.domain.permission.PermissionHandler
import com.vargag99.autoplaylist.domain.schedulers.Schedulers
import com.vargag99.autoplaylist.ui.generic.BaseViewModel
import com.vargag99.autoplaylist.ui.generic.Event
import com.vargag99.autoplaylist.ui.generic.combineLatest
import com.vargag99.autoplaylist.ui.generic.map
import com.vargag99.autoplaylist.ui.generic.postEvent
import com.vargag99.autoplaylist.ui.generic.readOnly
import com.vargag99.autoplaylist.ui.generic.toggle
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber

class InitViewModel(
    private val firstRunHandler: FirstRunHandler,
    private val permissionHandler: PermissionHandler,
    private val engine: Engine,
    private val schedulers: Schedulers,
    private val analytics: Analytics
) : BaseViewModel() {
    private val _navigation = MutableLiveData<Event<InitTarget>>()
    private val _storagePermissionOk = MutableLiveData<Boolean>()
    private val _numLists = MutableLiveData<NumLists>(NumLists.Unknown)
    private val _takeOverAccepted = MutableLiveData<Boolean>(false)

    val navigation = _navigation.readOnly()
    val numLists = _numLists.readOnly()
    val writeStorageState: LiveData<ButtonState> = _storagePermissionOk.map { storagePermissionsOk ->
        if (storagePermissionsOk) ButtonState.DONE else ButtonState.ENABLED
    }

    val takeOverState = combineLatest(_numLists, _takeOverAccepted) { numLists, takeOverAccepted ->
        when {
            numLists.isZero() || takeOverAccepted -> ButtonState.DONE
            numLists.isNonZero() -> ButtonState.ENABLED
            else -> ButtonState.DISABLED
        }
    }
    val doneEnabled = combineLatest(writeStorageState, takeOverState) { writeStorageState, takeOverState ->
        writeStorageState == ButtonState.DONE && takeOverState == ButtonState.DONE
    }

    init {
        checkPermissions()
    }

    private fun checkPermissions() {
        val permissionOk = permissionHandler.missingPermissions().isEmpty()
        _storagePermissionOk.value = permissionOk
        if (permissionOk) countLists()
    }

    fun initChecksDone(): Boolean =
        permissionHandler.missingPermissions().isEmpty() && firstRunHandler.isFirstRunDone()

    fun onWriteStorageClicked() {
        analytics.click("WriteStorage")
        permissionHandler.missingPermissions().takeIf { it.isNotEmpty() }?.let {
            _navigation.postEvent(InitTarget.RequestPermission(it))
        }
    }

    fun onRequestPermissionsResult(permissions: Array<String>, grantResults: IntArray) {
        for (result in permissions.zip(grantResults.toList())) {
            val event = if (result.second == PERMISSION_GRANTED) Events.PERMISSION_GRANT else Events.PERMISSION_DENY
            analytics.event(event, Properties.LABEL to result.first)
        }
        checkPermissions()
    }

    fun onTakeOverClicked() {
        analytics.click("TakeOver")
        _takeOverAccepted.toggle()
    }

    fun onDoneClicked() {
        analytics.click("Done")
        _navigation.postEvent(InitTarget.Main)
    }

    private fun countLists() {
        engine.count()
            .andThen(engine.numLists.firstOrError())
            .observeOn(schedulers.main)
            .doOnSubscribe { _numLists.value = NumLists.Counting }
            .subscribeBy(onError = ::handleError, onSuccess = ::handleCountResult)
            .addTo(disposables)
    }

    private fun handleCountResult(count: Int) {
        Timber.i("Found $count lists")
        _numLists.value = NumLists.Counted(count)
    }

    private fun handleError(throwable: Throwable) {
        Timber.e(throwable)
        _navigation.postEvent(InitTarget.Error(throwable))
    }
}
