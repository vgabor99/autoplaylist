package com.vargag99.autoplaylist.ui.generic

import androidx.lifecycle.toLiveData
import com.f2prateek.rx.preferences2.Preference
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable

fun <T> Preference<T>.toFlowable(strategy: BackpressureStrategy): Flowable<T> = this.asObservable().toFlowable(strategy)

fun <T> Preference<T>.toLiveData() = this.toFlowable(BackpressureStrategy.LATEST).toLiveData()
