package com.vargag99.autoplaylist.ui.info

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.vargag99.autoplaylist.R
import com.vargag99.autoplaylist.databinding.InfoActivityBinding
import com.vargag99.autoplaylist.domain.analytics.Analytics
import com.vargag99.autoplaylist.domain.appinfo.AppInfo
import com.vargag99.autoplaylist.ui.generic.consume
import com.vargag99.autoplaylist.util.exhaustive
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class InfoActivity : AppCompatActivity() {
    private val analytics: Analytics by inject()
    private val appInfo: AppInfo by inject()
    private val viewModel: InfoViewModel by viewModel()
    private lateinit var binding: InfoActivityBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.info_activity)
        binding.viewModel = viewModel
        binding.appInfo = appInfo
        binding.lifecycleOwner = this
        viewModel.navigation.consume(this) { target ->
            when (target) {
                InfoTarget.Back -> onBackPressed()
                InfoTarget.Rate -> rateInPlayMarket()
                InfoTarget.Share -> share()
            }.exhaustive
        }
    }

    override fun onResume() {
        super.onResume()
        analytics.screen(this, "Info")
    }

    override fun onPause() {
        super.onPause()
        if (isFinishing) overridePendingTransition(R.anim.no_animation, R.anim.exit_to_right)
    }

    private fun rateInPlayMarket() {
        val uri = Uri.parse("market://details?id=$packageName")
        val goToMarket = Intent(Intent.ACTION_VIEW, uri)
        // To count with Play Store backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY or
            Intent.FLAG_ACTIVITY_NEW_DOCUMENT or
            Intent.FLAG_ACTIVITY_MULTIPLE_TASK)
        try {
            startActivity(goToMarket)
        } catch (e: ActivityNotFoundException) {
            // Play Store app not found, try a browser.
            try {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(playStoreHttpUri)))
            } catch (e1: Exception) {
                Timber.e(e1, "rate failed")
            }
        }
    }

    private fun share() {
        val appLabel = appInfo.appLabel
        val uri = playStoreHttpUri
        val message = getString(R.string.share_message_appname_url, appLabel, uri)
        val sendIntent = Intent().apply {
            action = Intent.ACTION_SEND
            type = "text/plain"
            putExtra(Intent.EXTRA_TEXT, message)
            putExtra(Intent.EXTRA_SUBJECT, appLabel)
        }
        startActivity(Intent.createChooser(sendIntent, getText(R.string.share_chooser_title)))
    }

    private val playStoreHttpUri: String
        get() = "http://play.google.com/store/apps/details?id=$packageName"
}
