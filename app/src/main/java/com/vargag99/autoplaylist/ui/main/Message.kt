package com.vargag99.autoplaylist.ui.main

sealed class Message {
    object Working : Message()
    object Disabled : Message()
    class Enabled(val numLists: Int) : Message()
}
