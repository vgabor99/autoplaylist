package com.vargag99.autoplaylist.ui.init

import android.graphics.drawable.Drawable
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.google.android.material.button.MaterialButton
import com.vargag99.autoplaylist.R

@BindingAdapter("takeOverMessage")
fun bindTakeOverMessage(view: TextView, numLists: NumLists?) {
    view.text = when (numLists) {
        null -> null
        NumLists.Unknown -> view.context.getString(R.string.permission_takeover_message_files_first)
        NumLists.Counting -> view.context.getString(R.string.permission_takeover_message_counting)
        is NumLists.Counted -> if (numLists.count == 0) {
            view.context.getString(R.string.permission_takeover_message_no_lists)
        } else {
            view.context.getString(R.string.permission_takeover_message_num_lists, numLists.count)
        }
    }
}

@Suppress("LongParameterList")
@BindingAdapter("buttonState", "enabledText", "disabledText", "doneText", "doneIcon", requireAll = true)
fun bindButtonState(
    view: MaterialButton,
    buttonState: ButtonState,
    enabledText: String,
    disabledText: String,
    doneText: String,
    doneIcon: Drawable) {
    when (buttonState) {
        ButtonState.DISABLED -> {
            view.isEnabled = false
            view.isChecked = false
            view.isClickable = false
            view.icon = null
            view.text = disabledText
        }
        ButtonState.ENABLED -> {
            view.isEnabled = true
            view.isChecked = false
            view.isClickable = true
            view.icon = null
            view.text = enabledText
        }
        ButtonState.DONE -> {
            view.isEnabled = true
            view.isChecked = true
            view.isClickable = false
            view.icon = doneIcon
            view.text = doneText
        }
    }
}
