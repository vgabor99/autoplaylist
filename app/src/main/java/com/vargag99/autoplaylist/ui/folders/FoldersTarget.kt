package com.vargag99.autoplaylist.ui.folders

import com.vargag99.autoplaylist.domain.folders.UnsupportedTreeUriException
import com.vargag99.autoplaylist.domain.folders.UnsupportedVolumeException

sealed class FoldersTarget {
    object Back : FoldersTarget()
    object PickFolder : FoldersTarget()
    sealed class Error(val throwable: Throwable) : FoldersTarget() {
        class UnsupportedFolderError(throwable: UnsupportedTreeUriException) : Error(throwable)
        class UnsuportedVolumeError(throwable: UnsupportedVolumeException) : Error(throwable)
        class Generic(throwable: Throwable) : Error(throwable)
    }
}
