package com.vargag99.autoplaylist.ui.info

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.vargag99.autoplaylist.domain.analytics.Analytics
import com.vargag99.autoplaylist.domain.analytics.click
import com.vargag99.autoplaylist.ui.generic.Event
import com.vargag99.autoplaylist.ui.generic.postEvent
import com.vargag99.autoplaylist.ui.generic.readOnly

class InfoViewModel(
    private val analytics: Analytics
) : ViewModel() {
    private val _navigation = MutableLiveData<Event<InfoTarget>>()
    val navigation = _navigation.readOnly()

    fun onBackClicked() {
        analytics.click("Back")
        _navigation.postEvent(InfoTarget.Back)
    }

    fun onRateClicked() {
        analytics.click("Rate")
        _navigation.postEvent(InfoTarget.Rate)
    }

    fun onShareClicked() {
        analytics.click("Share")
        _navigation.postEvent(InfoTarget.Share)
    }
}
