package com.vargag99.autoplaylist.ui.init

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.vargag99.autoplaylist.R
import com.vargag99.autoplaylist.databinding.InitActivityBinding
import com.vargag99.autoplaylist.domain.analytics.Analytics
import com.vargag99.autoplaylist.domain.analytics.Properties
import com.vargag99.autoplaylist.domain.analytics.alert
import com.vargag99.autoplaylist.domain.analytics.click
import com.vargag99.autoplaylist.domain.appinfo.AppInfo
import com.vargag99.autoplaylist.ui.generic.consume
import com.vargag99.autoplaylist.ui.main.MainActivity
import com.vargag99.autoplaylist.util.exhaustive
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class InitActivity : AppCompatActivity() {
    private val analytics: Analytics by inject()
    private val appInfo: AppInfo by inject()
    private val viewModel: InitViewModel by viewModel()
    private lateinit var binding: InitActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (viewModel.initChecksDone()) {
            navigateToMain(false)
        } else {
            binding = DataBindingUtil.setContentView(this, R.layout.init_activity)
            binding.viewModel = viewModel
            binding.lifecycleOwner = this
            viewModel.navigation.consume(this) { target ->
                when (target) {
                    is InitTarget.RequestPermission -> requestPermissions(target.permissions)
                    InitTarget.Main -> navigateToMain(true)
                    is InitTarget.Error -> showErrorDialog(target.throwable)
                }.exhaustive
            }
        }
    }

    override fun onResume() {
        super.onResume()
        analytics.screen(this, "Init")
    }

    private fun requestPermissions(permissions: List<String>) {
        if (permissions.any { ActivityCompat.shouldShowRequestPermissionRationale(this, it) }) {
            showRationale(permissions)
        } else {
            doRequestPermissions(permissions)
        }
    }

    private fun showRationale(permissions: List<String>) {
        analytics.alert("Rationale")
        MaterialAlertDialogBuilder(this@InitActivity)
            .setTitle(R.string.permission_files_rationale_title)
            .setMessage(getString(R.string.permission_files_rationale_message, appInfo.appLabel))
            .setPositiveButton(android.R.string.ok) { _, _ ->
                analytics.click("OK")
                doRequestPermissions(permissions)
            }
            .create()
            .show()
    }

    private fun doRequestPermissions(permissions: List<String>) {
        ActivityCompat.requestPermissions(this, permissions.toTypedArray(), PERMISSION_REQUEST_CODE)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        viewModel.onRequestPermissionsResult(permissions, grantResults)
    }

    private fun navigateToMain(animate: Boolean) {
        startActivity(Intent(this@InitActivity, MainActivity::class.java))
        finish()
        if (animate) overridePendingTransition(R.anim.enter_from_right, R.anim.no_animation)
    }

    private fun showErrorDialog(throwable: Throwable) {
        val message = getString(R.string.error_dialog_message, throwable.toString())
        analytics.alert("Error", Properties.MESSAGE to message)
        MaterialAlertDialogBuilder(this@InitActivity)
            .setTitle(R.string.error_dialog_title)
            .setMessage(message)
            .setPositiveButton(android.R.string.ok) { _, _ ->
                analytics.click("OK")
            }
            .create()
            .show()
    }
}

private const val PERMISSION_REQUEST_CODE = 234
