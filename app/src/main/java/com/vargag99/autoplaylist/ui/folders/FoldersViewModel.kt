package com.vargag99.autoplaylist.ui.folders

import android.net.Uri
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.toLiveData
import com.vargag99.autoplaylist.domain.analytics.Analytics
import com.vargag99.autoplaylist.domain.analytics.Events
import com.vargag99.autoplaylist.domain.analytics.click
import com.vargag99.autoplaylist.domain.folders.Folder
import com.vargag99.autoplaylist.domain.folders.FolderRepo
import com.vargag99.autoplaylist.domain.folders.UnsupportedTreeUriException
import com.vargag99.autoplaylist.domain.folders.UnsupportedVolumeException
import com.vargag99.autoplaylist.domain.service.ServiceCommands
import com.vargag99.autoplaylist.ui.generic.BaseViewModel
import com.vargag99.autoplaylist.ui.generic.Event
import com.vargag99.autoplaylist.ui.generic.postEvent
import com.vargag99.autoplaylist.ui.generic.readOnly
import timber.log.Timber

class FoldersViewModel(
    private val folderRepo: FolderRepo,
    private val serviceCommands: ServiceCommands,
    private val analytics: Analytics
) : BaseViewModel() {
    val folders = folderRepo.getFoldersFlowable().map(::toFolderItems).toLiveData()
    private val _navigation = MutableLiveData<Event<FoldersTarget>>()
    val navigation = _navigation.readOnly()

    private fun toFolderItems(folders: List<Folder>): List<FoldersItem> {
        return if (folders.isEmpty()) {
            listOf(FoldersItem.Default, FoldersItem.Select)
        } else {
            folders.map { FoldersItem.Folder(it) }.sortedBy { it.folder.treeUri.file.name } + FoldersItem.Add
        }
    }

    fun onDirectoryPicked(uri: Uri) = try {
        analytics.event(Events.DIRECTORY_PICK)
        folderRepo.addFolder(uri)
        serviceCommands.update()
    } catch (exception: Exception) {
        handleError(exception)
    }

    fun onBackClicked() {
        analytics.click("Back")
        _navigation.postEvent(FoldersTarget.Back)
    }

    fun onItemClicked(item: FoldersItem.Default) {
        analytics.click("DefaultFolders")
        _navigation.postEvent(FoldersTarget.PickFolder)
    }

    fun onItemClicked(item: FoldersItem.Select) {
        analytics.click("SelectFolders")
        _navigation.postEvent(FoldersTarget.PickFolder)
    }

    fun onItemClicked(item: FoldersItem.Add) {
        analytics.click("AddFolder")
        _navigation.postEvent(FoldersTarget.PickFolder)
    }

    fun onItemClicked(item: FoldersItem.Folder) {
        analytics.click("FolderItem")
    }

    fun onDeleteClicked(item: FoldersItem.Folder) = try {
        analytics.click("Delete")
        folderRepo.deleteFolder(item.folder)
        serviceCommands.update()
    } catch (exception: Exception) {
        handleError(exception)
    }

    private fun handleError(throwable: Throwable) {
        Timber.e(throwable)
        val error = when (throwable) {
            is UnsupportedTreeUriException -> FoldersTarget.Error.UnsupportedFolderError(throwable)
            is UnsupportedVolumeException -> FoldersTarget.Error.UnsuportedVolumeError(throwable)
            else -> FoldersTarget.Error.Generic(throwable)
        }
        _navigation.postEvent(error)
    }
}
