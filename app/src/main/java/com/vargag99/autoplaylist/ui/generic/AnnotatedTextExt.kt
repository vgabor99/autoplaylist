package com.vargag99.autoplaylist.ui.generic

import android.graphics.Typeface
import android.text.Annotation
import android.text.Spannable
import android.text.SpannableString
import android.text.SpannedString
import android.text.style.StyleSpan

fun CharSequence.annotated(): CharSequence {
    val annotations = getAnnotations() ?: return this
    return SpannableString(this).apply { applyAnnotations(annotations) }
}

fun CharSequence.getAnnotations(): Array<Annotation>? =
    (this as? SpannedString)?.getSpans(0, length, Annotation::class.java)

fun SpannableString.applyAnnotations(annotations: Array<Annotation>) {
    for (annotation in annotations) {
        annotation.toSpan()?.let {
            setSpan(
                it,
                getSpanStart(annotation),
                getSpanEnd(annotation),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        }
    }
}

fun Annotation.toSpan(): Any? =
    when (key) {
        "style" ->
            when (value) {
                "bold" -> StyleSpan(Typeface.BOLD)
                else -> null
            }
        else -> null
    }
