package com.vargag99.autoplaylist.ui.info

import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.vargag99.autoplaylist.R
import com.vargag99.autoplaylist.domain.appinfo.AppInfo
import java.util.Calendar

@BindingAdapter("copyrightMessage")
fun bindCopyrightMessage(view: TextView, unused: Boolean) {
    view.text = view.context.getString(R.string.copyright, Calendar.getInstance().get(Calendar.YEAR))
}

@BindingAdapter("infoMessage")
fun bindCopyrightMessage(view: TextView, appInfo: AppInfo) {
    view.text = view.context.getString(R.string.info_message, appInfo.appLabel)
}
