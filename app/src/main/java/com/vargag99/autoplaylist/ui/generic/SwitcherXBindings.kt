package com.vargag99.autoplaylist.ui.generic

import androidx.databinding.BindingAdapter
import com.bitvale.switcher.SwitcherX

@BindingAdapter("switcherChecked")
fun bindSwitcherChecked(switcher: SwitcherX, checked: Boolean?) {
    checked ?: return
    switcher.setChecked(checked, withAnimation = true)
}

@BindingAdapter("switcherOnCheckedChanged")
fun bindSwitcherCheckedChanged(switcher: SwitcherX, onCheckedChangeListener: BooleanBindingConsumer) {
    switcher.setOnCheckedChangeListener { onCheckedChangeListener.invoke(it) }
}
