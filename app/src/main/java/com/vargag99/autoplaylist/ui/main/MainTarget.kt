package com.vargag99.autoplaylist.ui.main

sealed class MainTarget {
    object InternalOnlyAlert : MainTarget()
    object ShowLists : MainTarget()
    object Folders : MainTarget()
    object Info : MainTarget()
}
