package com.vargag99.autoplaylist.ui.generic

import android.widget.TextView
import androidx.databinding.BindingAdapter

@BindingAdapter("annotatedText")
fun bindAnnotatedText(view: TextView, resId: Int) {
    view.text = view.context.getText(resId).annotated()
}
