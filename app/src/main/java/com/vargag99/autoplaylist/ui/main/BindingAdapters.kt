package com.vargag99.autoplaylist.ui.main

import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.transition.Slide
import androidx.transition.TransitionManager
import com.vargag99.autoplaylist.R

@BindingAdapter("mainMessage")
fun bindMainMessage(view: TextView, message: Message?) {
    view.text = when (message) {
        null -> null
        Message.Working -> view.context.getString(R.string.working)
        Message.Disabled -> view.context.getString(R.string.all_lists_removed)
        is Message.Enabled -> view.context.getString(R.string.num_lists_generated, message.numLists)
    }
}

@BindingAdapter("slidingVisibility")
fun setSlidingVisibility(view: View, isVisible: Boolean) {
    TransitionManager.beginDelayedTransition(view.rootView as ViewGroup, Slide().apply { slideEdge = Gravity.BOTTOM })
    view.visibility = if (isVisible) View.VISIBLE else View.INVISIBLE
}
