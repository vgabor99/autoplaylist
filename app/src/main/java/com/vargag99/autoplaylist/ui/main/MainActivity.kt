package com.vargag99.autoplaylist.ui.main

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.vargag99.autoplaylist.R
import com.vargag99.autoplaylist.databinding.MainActivityBinding
import com.vargag99.autoplaylist.domain.analytics.Analytics
import com.vargag99.autoplaylist.domain.analytics.alert
import com.vargag99.autoplaylist.domain.analytics.click
import com.vargag99.autoplaylist.ui.folders.FoldersActivity
import com.vargag99.autoplaylist.ui.generic.consume
import com.vargag99.autoplaylist.ui.info.InfoActivity
import com.vargag99.autoplaylist.util.exhaustive
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class MainActivity : AppCompatActivity() {
    private val analytics: Analytics by inject()
    private val viewModel: MainViewModel by viewModel()
    private lateinit var binding: MainActivityBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.main_activity)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        viewModel.navigation.consume(this) { target ->
            when (target) {
                MainTarget.InternalOnlyAlert -> showInternalOnlyAlert()
                MainTarget.ShowLists -> showLists()
                MainTarget.Folders -> navigateToFolders()
                MainTarget.Info -> navigateToInfo()
            }.exhaustive
        }
    }

    override fun onResume() {
        super.onResume()
        analytics.screen(this, "Main")
    }

    private fun showInternalOnlyAlert() {
        analytics.alert("InternalOnlyAlert")
        MaterialAlertDialogBuilder(this@MainActivity)
            .setTitle(R.string.error_dialog_title_internal_only)
            .setMessage(R.string.error_dialog_message_internal_only)
            .setPositiveButton(android.R.string.ok) { _, _ ->
                analytics.click("OK")
                viewModel.onInternalOnlyAlertOk()
            }
            .create()
            .show()
    }

    private fun showLists() = try {
        val intent = Intent.makeMainSelectorActivity(Intent.ACTION_MAIN, Intent.CATEGORY_APP_MUSIC)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    } catch (exception: Exception) {
        Timber.e(exception, "Failed to start music player")
    }

    private fun navigateToFolders() {
        startActivity(Intent(this@MainActivity, FoldersActivity::class.java))
        overridePendingTransition(R.anim.enter_from_left, R.anim.no_animation)
    }

    private fun navigateToInfo() {
        startActivity(Intent(this@MainActivity, InfoActivity::class.java))
        overridePendingTransition(R.anim.enter_from_right, R.anim.no_animation)
    }
}
