package com.vargag99.autoplaylist.ui.info

sealed class InfoTarget {
    object Back : InfoTarget()
    object Rate : InfoTarget()
    object Share : InfoTarget()
}
