package com.vargag99.autoplaylist.ui.main

import android.os.Build
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.toLiveData
import com.vargag99.autoplaylist.domain.analytics.Analytics
import com.vargag99.autoplaylist.domain.analytics.click
import com.vargag99.autoplaylist.domain.autoupdates.UpdateScheduler
import com.vargag99.autoplaylist.domain.engine.Engine
import com.vargag99.autoplaylist.domain.engine.MediaScannerObserver
import com.vargag99.autoplaylist.domain.firstrun.FirstRunHandler
import com.vargag99.autoplaylist.domain.preferences.Preferences
import com.vargag99.autoplaylist.domain.service.ServiceCommands
import com.vargag99.autoplaylist.ui.generic.Event
import com.vargag99.autoplaylist.ui.generic.postEvent
import com.vargag99.autoplaylist.ui.generic.readOnly
import com.vargag99.autoplaylist.ui.generic.toFlowable
import com.vargag99.autoplaylist.ui.generic.toLiveData
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.rxkotlin.Flowables
import timber.log.Timber

@Suppress("LongParameterList")
class MainViewModel(
    private val engine: Engine,
    private val serviceCommands: ServiceCommands,
    private val updateScheduler: UpdateScheduler,
    private val preferences: Preferences,
    firstRunHandler: FirstRunHandler,
    private val analytics: Analytics,
    mediaScannerObserver: MediaScannerObserver
) : ViewModel() {
    private val _navigation = MutableLiveData<Event<MainTarget>>()
    val navigation = _navigation.readOnly()
    val enabled: LiveData<Boolean> = preferences.enabled.toLiveData()
    val message: LiveData<Message> = getMessage().toLiveData()
    val refreshEnabled: LiveData<Boolean> = engine.state.map { it == Engine.State.IDLE }.toLiveData()
    val mediaScannerRunning = mediaScannerObserver.isMediaScannerRunningFlowable.toLiveData()

    init {
        if (!firstRunHandler.triggerFirstRun()) {
            // First run counts anyway, so why count it again
            serviceCommands.count()
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            if (!preferences.internalOnlyAlertShown.get()) {
                _navigation.postEvent(MainTarget.InternalOnlyAlert)
            }
        }
    }

    private fun getMessage(): Flowable<Message> = Flowables.combineLatest(
        engine.state,
        engine.numLists,
        preferences.enabled.toFlowable(BackpressureStrategy.LATEST)) { state, numLists, enabled ->
        when (state) {
            Engine.State.IDLE -> if (enabled) Message.Enabled(numLists) else Message.Disabled
            else -> Message.Working
        }
    }

    fun onRefreshClicked() {
        analytics.click("Refresh")
        serviceCommands.update()
    }

    fun onShowListsClicked() {
        analytics.click("ShowLists")
        _navigation.postEvent(MainTarget.ShowLists)
    }

    fun onFoldersClicked() {
        analytics.click("Folders")
        _navigation.postEvent(MainTarget.Folders)
    }

    fun onInfoClicked() {
        analytics.click("Info")
        _navigation.postEvent(MainTarget.Info)
    }

    fun onEnabledChanged(isChecked: Boolean) {
        Timber.i("Enabled: $isChecked")
        analytics.click(if (isChecked) "TurnOn" else "TurnOff")
        preferences.enabled.set(isChecked)
        serviceCommands.update()
        updateScheduler.scheduleUpdatesAsNeeded()
    }

    fun onInternalOnlyAlertOk() {
        preferences.internalOnlyAlertShown.set(true)
    }
}
