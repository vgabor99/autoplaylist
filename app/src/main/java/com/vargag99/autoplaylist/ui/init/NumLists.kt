package com.vargag99.autoplaylist.ui.init

sealed class NumLists {
    object Unknown : NumLists()
    object Counting : NumLists()
    class Counted(val count: Int) : NumLists()
}

fun NumLists.isZero() = this is NumLists.Counted && count == 0
fun NumLists.isNonZero() = this is NumLists.Counted && count > 0
