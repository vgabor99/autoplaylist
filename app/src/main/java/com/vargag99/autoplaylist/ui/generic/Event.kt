package com.vargag99.autoplaylist.ui.generic

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer

class Event<out T>(private val content: T) {

    @Suppress("WeakerAccess")
    var consumed = false
        private set

    fun consume(): T? {
        return if (consumed) {
            null
        } else {
            consumed = true
            content
        }
    }

    @Suppress("Unused")
    fun peek(): T = content
}

fun <T : Any> T.asEvent() = Event(this)
@Suppress("Unused")
fun <T : Any> MutableLiveData<Event<T>>.setEvent(value: T) = setValue(value.asEvent())

fun <T : Any> MutableLiveData<Event<T>>.postEvent(value: T) = postValue(value.asEvent())
fun <T : Any> LiveData<Event<T>>.consume(owner: LifecycleOwner, block: (T) -> Unit) =
    observe(owner, Observer { event ->
        event.consume()?.let { block(it) }
    })
