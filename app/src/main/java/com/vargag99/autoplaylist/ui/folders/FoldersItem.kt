package com.vargag99.autoplaylist.ui.folders

import com.vargag99.autoplaylist.domain.folders.Folder as DomainFolder

sealed class FoldersItem {
    object Default : FoldersItem()
    object Select : FoldersItem()
    data class Folder(val folder: DomainFolder) : FoldersItem()
    object Add : FoldersItem()
}
