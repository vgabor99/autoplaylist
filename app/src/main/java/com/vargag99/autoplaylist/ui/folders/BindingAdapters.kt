package com.vargag99.autoplaylist.ui.folders

import android.graphics.drawable.Drawable
import android.os.Build
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.vargag99.autoplaylist.R

@BindingAdapter("dividerDecoration")
fun bindDividerDecoration(view: RecyclerView, drawable: Drawable?) {
    val layoutManager = view.layoutManager as? LinearLayoutManager ?: return
    val decoration = DividerItemDecoration(view.context, layoutManager.orientation)
    if (drawable != null) decoration.setDrawable(drawable)
    view.addItemDecoration(decoration)
}

@BindingAdapter("defaultFolderTitle")
fun bindDefaultFolderTitle(view: TextView, unused: Boolean) {
    view.text = view.context.getString(
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) R.string.default_folders_title
        else R.string.default_folders_title_q
    )
}
