package com.vargag99.autoplaylist.ui.init

enum class ButtonState { DISABLED, ENABLED, DONE }
