package com.vargag99.autoplaylist.ui

import com.vargag99.autoplaylist.ui.folders.FoldersViewModel
import com.vargag99.autoplaylist.ui.info.InfoViewModel
import com.vargag99.autoplaylist.ui.init.InitViewModel
import com.vargag99.autoplaylist.ui.main.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val uiModule = module {
    viewModel { MainViewModel(get(), get(), get(), get(), get(), get(), get()) }
    viewModel { InitViewModel(get(), get(), get(), get(), get()) }
    viewModel { InfoViewModel(get()) }
    viewModel { FoldersViewModel(get(), get(), get()) }
}
