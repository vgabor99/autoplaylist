package com.vargag99.autoplaylist.domainimpl.autoupdates.uritrigger

import android.annotation.TargetApi
import android.os.Build
import android.provider.MediaStore
import androidx.work.Constraints
import androidx.work.ExistingWorkPolicy
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import com.vargag99.autoplaylist.domain.autoupdates.UpdateScheduler
import com.vargag99.autoplaylist.domain.preferences.Preferences
import com.vargag99.autoplaylist.domainimpl.autoupdates.UpdaterWorker
import com.vargag99.autoplaylist.domainimpl.autoupdates.UpdaterWorker.Companion.INITIAL_DELAY_SECONDS
import timber.log.Timber
import java.util.concurrent.TimeUnit

class UriTriggerUpdateScheduler(
    private val workManager: WorkManager,
    private val preferences: Preferences
) : UpdateScheduler, UpdaterWorker.Listener {
    override fun scheduleUpdatesAsNeeded() {
        Timber.d("scheduleUpdatesAsNeeded")
        if (preferences.enabled.get()) scheduleUpdates() else cancelUpdates()
    }

    @TargetApi(Build.VERSION_CODES.N)
    private fun scheduleUpdates() {
        Timber.i("scheduleUpdates")
        val workRequest = OneTimeWorkRequest.Builder(UpdaterWorker::class.java)
            .setConstraints(Constraints.Builder()
                .addContentUriTrigger(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, true)
                .addContentUriTrigger(MediaStore.getMediaScannerUri(), false)
                .build())
            .setInitialDelay(INITIAL_DELAY_SECONDS, TimeUnit.SECONDS)
            .build()
        workManager.enqueueUniqueWork(UpdaterWorker.NAME, ExistingWorkPolicy.REPLACE, workRequest)
    }

    private fun cancelUpdates() {
        Timber.i("cancelUpdates")
        workManager.cancelUniqueWork(UpdaterWorker.NAME)
    }

    override fun onUpdateWorkerCompleted() = scheduleUpdates()
}
