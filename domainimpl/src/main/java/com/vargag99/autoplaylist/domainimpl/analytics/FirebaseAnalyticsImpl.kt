package com.vargag99.autoplaylist.domainimpl.analytics

import android.app.Activity
import android.content.Context
import android.os.Bundle
import com.google.firebase.analytics.FirebaseAnalytics
import com.vargag99.autoplaylist.domain.analytics.Analytics

class FirebaseAnalyticsImpl(context: Context) : Analytics {
    private val analytics = FirebaseAnalytics.getInstance(context)

    override fun screen(activity: Activity, name: String) {
        analytics.setCurrentScreen(activity, name, name)
    }

    override fun event(event: String, vararg data: Pair<String, Any>) {
        val bundle = Bundle().apply {
            for ((key, value) in data) {
                when (value) {
                    is Boolean -> putBoolean(key, value)
                    is Byte -> putByte(key, value)
                    is Char -> putChar(key, value)
                    is Short -> putShort(key, value)
                    is Int -> putInt(key, value)
                    is Long -> putLong(key, value)
                    is Float -> putFloat(key, value)
                    is Double -> putDouble(key, value)
                    is String -> putString(key, value)
                    else -> putString(key, value.toString())
                }
            }
        }
        analytics.logEvent(event, bundle)
    }
}
