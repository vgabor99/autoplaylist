package com.vargag99.autoplaylist.domainimpl.autoupdates.mediascan

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import timber.log.Timber

class MediaScanReceiver : BroadcastReceiver(), KoinComponent {
    interface EventHandler {
        fun onMediaScannerFinished()
    }

    private val mediaScanEventHandler: EventHandler by inject()
    override fun onReceive(context: Context?, intent: Intent?) {
        Timber.d("onReceive $intent")
        mediaScanEventHandler.onMediaScannerFinished()
    }
}
