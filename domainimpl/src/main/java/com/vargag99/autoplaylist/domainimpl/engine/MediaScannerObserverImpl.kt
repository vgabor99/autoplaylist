package com.vargag99.autoplaylist.domainimpl.engine

import android.content.ContentResolver
import android.database.ContentObserver
import android.os.Handler
import android.provider.MediaStore
import com.vargag99.autoplaylist.domain.engine.MediaScannerObserver
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable

class MediaScannerObserverImpl(
    private val contentResolver: ContentResolver
) : MediaScannerObserver {

    override val isMediaScannerRunning: Boolean
        get() {
            contentResolver.query(
                MediaStore.getMediaScannerUri(), arrayOf(MediaStore.MEDIA_SCANNER_VOLUME),
                null, null, null)?.use { cursor ->
                if (cursor.moveToFirst()) {
                    val columnIndex = cursor.getColumnIndex(MediaStore.MEDIA_SCANNER_VOLUME)
                    val volumeName = cursor.getString(columnIndex)
                    if (volumeName != null) {
                        return true
                    }
                }
            }
            return false
        }

    override val isMediaScannerRunningFlowable: Flowable<Boolean> = Flowable.create<Boolean>(
        { emitter ->
            val handler = Handler()
            lateinit var poll: Runnable // ContentObserver is not called when the scanner stops, we have to poll :(
            fun schedulePoll() {
                handler.removeCallbacks(poll)
                handler.postDelayed(poll, POLL_PERIOD_MILLIS)
            }

            fun emit() {
                val value = isMediaScannerRunning
                if (emitter.isCancelled) return
                emitter.onNext(value)
                if (value) schedulePoll()
            }
            poll = Runnable { emit() }
            val contentObserver = object : ContentObserver(handler) {
                override fun onChange(selfChange: Boolean) = emit()
            }
            contentResolver.registerContentObserver(
                MediaStore.getMediaScannerUri(),
                false,
                contentObserver)
            emitter.setCancellable {
                handler.removeCallbacksAndMessages(null)
                contentResolver.unregisterContentObserver(contentObserver)
            }
            emit()
        },
        BackpressureStrategy.LATEST)
        .distinctUntilChanged()

    companion object {
        private const val POLL_PERIOD_MILLIS = 2000L
    }
}
