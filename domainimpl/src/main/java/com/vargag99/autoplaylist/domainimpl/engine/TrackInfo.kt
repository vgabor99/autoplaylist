package com.vargag99.autoplaylist.domainimpl.engine

import java.io.File

class TrackInfo private constructor(val id: Long, val fn: String, val dir: String) {
    override fun toString() = "TrackInfo {id=$id, fn=$fn, dir=$dir}"

    companion object {
        @Suppress("ReturnCount")
        fun from(id: Long, fn: String?): TrackInfo? {
            if (fn == null) return null
            val dir = File(fn).parent ?: return null
            return TrackInfo(id, fn, dir)
        }
    }
}

@Suppress("ReturnCount")
infix fun List<TrackInfo>.matches(other: List<TrackInfo>): Boolean {
    // Check if the two track lists match (same tracks in the same order).
    if (this.size != other.size) {
        return false
    }
    for (i in this.indices) {
        if (this[i].id != other[i].id) {
            return false
        }
    }
    return true
}

fun Iterable<TrackInfo>.getDir(): String? {
    // Get dir of tracks, if it is the same dir for all.
    // Returns null for multiple dirs.
    // Returns null for empty list.
    return firstOrNull()?.dir?.takeIf { dir -> none { it.dir != dir } }
}
