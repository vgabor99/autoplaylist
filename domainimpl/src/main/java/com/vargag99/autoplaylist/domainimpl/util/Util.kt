package com.vargag99.autoplaylist.domainimpl.util

import android.content.Context
import android.content.pm.PackageManager

object Util {
    fun appLabel(context: Context): String = try {
        val applicationInfo = context.packageManager.getApplicationInfo(context.applicationInfo.packageName, 0)
        context.packageManager.getApplicationLabel(applicationInfo).toString()
    } catch (e: PackageManager.NameNotFoundException) {
        e.printStackTrace()
        "Auto Playlist"
    }

    fun appVersionName(context: Context): String = try {
        context.packageManager.getPackageInfo(context.packageName, PackageManager.GET_META_DATA)
            .versionName
    } catch (e: PackageManager.NameNotFoundException) {
        e.printStackTrace()
        "0"
    }

    fun appVersionCode(context: Context): Int = try {
        context.packageManager.getPackageInfo(context.packageName, PackageManager.GET_META_DATA).versionCode
    } catch (e: PackageManager.NameNotFoundException) {
        e.printStackTrace()
        0
    }
}
