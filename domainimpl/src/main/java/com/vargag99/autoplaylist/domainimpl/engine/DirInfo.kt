package com.vargag99.autoplaylist.domainimpl.engine

data class DirInfo(val fn: String) {
    val tracks = mutableListOf<TrackInfo>() // Tracks in the dir, in order.
    var playListName: String? = null // Playlist name.
    var playListGenDepth: Int = 1 // Playlist name generation depth.
    var maxedOut: Boolean = false // True if increasing playListGenDepth no longer works.
    var playListId: Long = 0 // ID of GOOD playlist (==nothing to do), or 0 (==make new playlist).
}

fun Iterable<TrackInfo>.groupByDirs(): Map<String, DirInfo> {
    // Get all files and build the dir info map.
    // This corresponds to what we want to have as playlists in the db
    // (one playlist for each dir, files in this order).
    val dirs = mutableMapOf<String, DirInfo>()
    for (track in this) {
        dirs.getOrPut(track.dir, { DirInfo(track.dir) }).tracks.add(track)
    }
    for (dir in dirs.values) {
        dir.tracks.sortWith(Comparator { left, right ->
            left.fn.compareTo(right.fn)
        })
    }
    return dirs
}
