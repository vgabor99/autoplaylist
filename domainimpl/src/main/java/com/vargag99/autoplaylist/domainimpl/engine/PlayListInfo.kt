package com.vargag99.autoplaylist.domainimpl.engine

data class PlayListInfo(val id: Long, val name: String?) {
    override fun toString() = "PlayListInfo {id=$id, name=$name}"
}
