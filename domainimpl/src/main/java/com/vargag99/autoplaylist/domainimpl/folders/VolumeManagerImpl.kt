package com.vargag99.autoplaylist.domainimpl.folders

import android.os.storage.StorageManager
import com.vargag99.autoplaylist.domain.folders.Volume
import timber.log.Timber

class VolumeManagerImpl(private val storageManager: StorageManager) : VolumeManager {
    override fun availableVolumes(): List<Volume> {
        val volumes = mutableListOf<Volume>()
        try {
            // For your own mental well-being, please look away now.
            val storageVolumeClazz = Class.forName("android.os.storage.StorageVolume")
            val getVolumeList = storageManager.javaClass.getMethod("getVolumeList")
            val getUuid = storageVolumeClazz.getMethod("getUuid")
            val getPath = storageVolumeClazz.getMethod("getPath")
            val isPrimary = storageVolumeClazz.getMethod("isPrimary")
            val getUserLabel = storageVolumeClazz.getMethod("getUserLabel")
            val result = getVolumeList.invoke(storageManager) as Any
            val length = java.lang.reflect.Array.getLength(result)
            for (i in 0 until length) {
                val storageVolumeElement = java.lang.reflect.Array.get(result, i)
                val uuid = getUuid.invoke(storageVolumeElement) as String?
                val primary = isPrimary.invoke(storageVolumeElement) as Boolean
                val path = getPath.invoke(storageVolumeElement) as String?
                val description = getUserLabel.invoke(storageVolumeElement) as String?
                if (path != null) {
                    volumes.add(Volume(uuid = uuid, path = path, isPrimary = primary, description = description))
                }
            }
        } catch (throwable: Throwable) {
            Timber.e(throwable)
        }
        return volumes
    }
}
