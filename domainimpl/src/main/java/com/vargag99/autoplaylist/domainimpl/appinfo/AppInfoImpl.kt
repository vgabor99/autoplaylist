package com.vargag99.autoplaylist.domainimpl.appinfo

import android.content.Context
import com.vargag99.autoplaylist.domain.appinfo.AppInfo
import com.vargag99.autoplaylist.domainimpl.util.Util

class AppInfoImpl(context: Context) : AppInfo {
    override val appLabel = Util.appLabel(context)
    override val appVersionName = Util.appVersionName(context)
    override val appVersionCode = Util.appVersionCode(context)
}
