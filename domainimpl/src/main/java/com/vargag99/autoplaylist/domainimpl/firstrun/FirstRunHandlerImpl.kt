package com.vargag99.autoplaylist.domainimpl.firstrun

import com.vargag99.autoplaylist.domain.analytics.Analytics
import com.vargag99.autoplaylist.domain.analytics.Events
import com.vargag99.autoplaylist.domain.appinfo.AppInfo
import com.vargag99.autoplaylist.domain.autoupdates.UpdateScheduler
import com.vargag99.autoplaylist.domain.firstrun.FirstRunHandler
import com.vargag99.autoplaylist.domain.preferences.Preferences
import com.vargag99.autoplaylist.domain.service.ServiceCommands
import timber.log.Timber

class FirstRunHandlerImpl(
    private val preferences: Preferences,
    private val appInfo: AppInfo,
    private val serviceCommands: ServiceCommands,
    private val updateScheduler: UpdateScheduler,
    private val analytics: Analytics
) : FirstRunHandler {
    override fun isFirstRunDone(): Boolean = preferences.version.isSet
    override fun isAppVersionChanged(): Boolean = preferences.version.get() != appInfo.appVersionCode

    override fun triggerFirstRun(): Boolean = when {
        !isFirstRunDone() -> {
            Timber.i("First run")
            analytics.event(Events.FIRST_RUN)
            doFirstRun()
            true
        }
        isAppVersionChanged() -> {
            Timber.i("App version changed")
            analytics.event(Events.VERSION_CHANGED_RUN)
            doFirstRun()
            true
        }
        else -> false
    }

    private fun doFirstRun() {
        serviceCommands.update()
        updateScheduler.scheduleUpdatesAsNeeded()
        preferences.version.set(appInfo.appVersionCode)
    }
}
