package com.vargag99.autoplaylist.domainimpl.folders

import android.annotation.SuppressLint
import android.net.Uri
import android.provider.DocumentsContract
import com.vargag99.autoplaylist.domain.folders.FilePath
import com.vargag99.autoplaylist.domain.folders.Volume

class FilePathExtractorImpl(
    private val volumeManager: VolumeManager
) : FilePathExtractor {
    override fun getFilePath(uri: Uri): FilePath? =
        if (isExternalStorageDocument(uri)) getFullPathFromUri(uri) else null

    private fun getFullPathFromUri(uri: Uri): FilePath? {
        val volume = getVolume(getVolumeIdFromUri(uri)) ?: return null
        val documentPath = getDocumentPathFromUri(uri)
        return FilePath(volume = volume, path = documentPath)
    }

    @SuppressLint("ObsoleteSdkInt")
    private fun getVolume(volumeId: String?): Volume? {
        val volumes = volumeManager.availableVolumes()
        return when (volumeId) {
            null -> null
            PRIMARY_VOLUME_NAME -> volumes.find { it.isPrimary }
            else -> volumes.find { it.uuid == volumeId }
        }
    }

    private fun getVolumeIdFromUri(uri: Uri): String? =
        DocumentsContract.getTreeDocumentId(uri)
            .split(":")
            .getOrNull(0)

    private fun getDocumentPathFromUri(uri: Uri): String =
        if (isTreeUri(uri)) {
            DocumentsContract.getTreeDocumentId(uri)
        } else {
            DocumentsContract.getDocumentId(uri)
        }
            .split(":")
            .getOrElse(1) { "" }

    private fun isTreeUri(uri: Uri): Boolean {
        val paths = uri.pathSegments
        return paths.size >= 2 && paths[0] == "tree"
    }

    private fun isExternalStorageDocument(uri: Uri): Boolean =
        "com.android.externalstorage.documents" == uri.authority
}

private const val PRIMARY_VOLUME_NAME = "primary"
