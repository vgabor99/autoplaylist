package com.vargag99.autoplaylist.domainimpl

import android.os.Build
import com.vargag99.autoplaylist.domain.analytics.Analytics
import com.vargag99.autoplaylist.domain.appinfo.AppInfo
import com.vargag99.autoplaylist.domain.autoupdates.UpdateScheduler
import com.vargag99.autoplaylist.domain.engine.Engine
import com.vargag99.autoplaylist.domain.engine.MediaScannerObserver
import com.vargag99.autoplaylist.domain.firstrun.FirstRunHandler
import com.vargag99.autoplaylist.domain.folders.FolderRepo
import com.vargag99.autoplaylist.domain.permission.PermissionHandler
import com.vargag99.autoplaylist.domain.preferences.Preferences
import com.vargag99.autoplaylist.domain.schedulers.Schedulers
import com.vargag99.autoplaylist.domain.service.ServiceCommands
import com.vargag99.autoplaylist.domainimpl.analytics.FirebaseAnalyticsImpl
import com.vargag99.autoplaylist.domainimpl.appinfo.AppInfoImpl
import com.vargag99.autoplaylist.domainimpl.autoupdates.UpdaterWorker
import com.vargag99.autoplaylist.domainimpl.autoupdates.mediascan.MediaScanBroadcastEnabler
import com.vargag99.autoplaylist.domainimpl.autoupdates.mediascan.MediaScanBroadcastEnablerImpl
import com.vargag99.autoplaylist.domainimpl.autoupdates.mediascan.MediaScanReceiver
import com.vargag99.autoplaylist.domainimpl.autoupdates.mediascan.MediaScanUpdateScheduler
import com.vargag99.autoplaylist.domainimpl.autoupdates.uritrigger.UriTriggerUpdateScheduler
import com.vargag99.autoplaylist.domainimpl.engine.EngineImpl
import com.vargag99.autoplaylist.domainimpl.engine.FilterProvider
import com.vargag99.autoplaylist.domainimpl.engine.MediaContentHandler
import com.vargag99.autoplaylist.domainimpl.engine.MediaContentHandlerImpl
import com.vargag99.autoplaylist.domainimpl.engine.MediaScannerObserverImpl
import com.vargag99.autoplaylist.domainimpl.firstrun.FirstRunHandlerImpl
import com.vargag99.autoplaylist.domainimpl.folders.FilePathExtractor
import com.vargag99.autoplaylist.domainimpl.folders.FilePathExtractorImpl
import com.vargag99.autoplaylist.domainimpl.folders.FolderRepoImpl
import com.vargag99.autoplaylist.domainimpl.folders.TreeUriRepo
import com.vargag99.autoplaylist.domainimpl.folders.TreeUriRepoImpl
import com.vargag99.autoplaylist.domainimpl.folders.VolumeManager
import com.vargag99.autoplaylist.domainimpl.folders.VolumeManagerImpl
import com.vargag99.autoplaylist.domainimpl.permission.PermissionHandlerImpl
import com.vargag99.autoplaylist.domainimpl.preferences.PreferencesImpl
import com.vargag99.autoplaylist.domainimpl.schedulers.SchedulersImpl
import com.vargag99.autoplaylist.domainimpl.service.ServiceCommandsImpl
import org.koin.dsl.bind
import org.koin.dsl.binds
import org.koin.dsl.module

val domainModule = module {
    single<Schedulers> { SchedulersImpl() }
    factory<AppInfo> { AppInfoImpl(get()) }
    single<Analytics> { FirebaseAnalyticsImpl(get()) }
    single<Engine> { EngineImpl(get(), get(), get(), get(), get()) }
    factory<ServiceCommands> { ServiceCommandsImpl(get()) }
    single<Preferences> { PreferencesImpl(get()) }
    factory<MediaContentHandler> { MediaContentHandlerImpl(get(), get()) }
    factory<PermissionHandler> { PermissionHandlerImpl(get()) }
    factory<FirstRunHandler> { FirstRunHandlerImpl(get(), get(), get(), get(), get()) }
    single<TreeUriRepo> { TreeUriRepoImpl(get(), get()) }
    factory<FilePathExtractor> { FilePathExtractorImpl(get()) }
    factory<VolumeManager> { VolumeManagerImpl(get()) }
    factory<FolderRepo> { FolderRepoImpl(get(), get(), get()) } bind FilterProvider::class
    factory<MediaScannerObserver> { MediaScannerObserverImpl(get()) }
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        factory<UpdateScheduler> { UriTriggerUpdateScheduler(get(), get()) } bind UpdaterWorker.Listener::class
    } else {
        factory<UpdateScheduler> { MediaScanUpdateScheduler(get(), get(), get()) } binds arrayOf(
            UpdaterWorker.Listener::class,
            MediaScanReceiver.EventHandler::class
        )
        factory<MediaScanBroadcastEnabler> { MediaScanBroadcastEnablerImpl(get()) }
    }
}
