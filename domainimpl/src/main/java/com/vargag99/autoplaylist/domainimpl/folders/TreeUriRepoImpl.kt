package com.vargag99.autoplaylist.domainimpl.folders

import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.content.UriPermission
import android.net.Uri
import androidx.documentfile.provider.DocumentFile
import com.vargag99.autoplaylist.domain.folders.TreeUri
import com.vargag99.autoplaylist.domain.folders.toCaching
import io.reactivex.Flowable
import io.reactivex.processors.BehaviorProcessor
import timber.log.Timber

class TreeUriRepoImpl(
    private val context: Context,
    private val contentResolver: ContentResolver
) : TreeUriRepo {
    private val treeUris = BehaviorProcessor.createDefault<List<TreeUri>>(getData())

    override fun getTreeUris(): List<TreeUri> = treeUris.value ?: emptyList()

    override fun getTreeUrisFlowable(): Flowable<List<TreeUri>> = treeUris.hide()

    override fun deleteTreeUri(treeUri: TreeUri) {
        try {
            contentResolver.releasePersistableUriPermission(treeUri.treeUri, Intent.FLAG_GRANT_READ_URI_PERMISSION)
        } catch (e: Exception) {
            // If the folder is removed, the persistable permission is lost too. Simply refresh
            Timber.w(e)
        }
        refresh()
    }

    override fun addTreeUri(uri: Uri) {
        contentResolver.takePersistableUriPermission(uri, Intent.FLAG_GRANT_READ_URI_PERMISSION)
        refresh()
    }

    private fun getData() =
        contentResolver.persistedUriPermissions.map { it.toTreeUri() }

    private fun refresh() {
        val data = getData()
        Timber.i("${data.size} folders selected")
        treeUris.offer(data)
    }

    private fun UriPermission.toTreeUri() =
        TreeUri(treeUri = uri, file = requireNotNull(DocumentFile.fromTreeUri(context, uri)).toCaching())
}
