package com.vargag99.autoplaylist.domainimpl.folders

import com.vargag99.autoplaylist.domain.folders.Volume

interface VolumeManager {
    fun availableVolumes(): List<Volume>
}
