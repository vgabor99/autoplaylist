package com.vargag99.autoplaylist.domainimpl.autoupdates.mediascan

interface MediaScanBroadcastEnabler {
    fun enable()
    fun disable()
}
