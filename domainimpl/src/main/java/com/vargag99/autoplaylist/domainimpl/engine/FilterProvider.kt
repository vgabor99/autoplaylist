package com.vargag99.autoplaylist.domainimpl.engine

interface FilterProvider {
    fun getPaths(): List<String>
}
