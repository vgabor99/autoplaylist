package com.vargag99.autoplaylist.domainimpl.autoupdates.mediascan

import android.content.ComponentName
import android.content.Context
import android.content.pm.PackageManager
import timber.log.Timber

class MediaScanBroadcastEnablerImpl(private val context: Context) : MediaScanBroadcastEnabler {
    override fun enable() = setComponentEnabled(PackageManager.COMPONENT_ENABLED_STATE_ENABLED)
    override fun disable() = setComponentEnabled(PackageManager.COMPONENT_ENABLED_STATE_DISABLED)
    private fun setComponentEnabled(newState: Int) {
        Timber.d("setComponentEnabled $newState")
        val componentName = ComponentName(context, MediaScanReceiver::class.java)
        context.packageManager.setComponentEnabledSetting(componentName, newState, PackageManager.DONT_KILL_APP)
    }
}
