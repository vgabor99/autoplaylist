package com.vargag99.autoplaylist.domainimpl.autoupdates.mediascan

import androidx.work.ExistingWorkPolicy
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import com.vargag99.autoplaylist.domain.autoupdates.UpdateScheduler
import com.vargag99.autoplaylist.domain.preferences.Preferences
import com.vargag99.autoplaylist.domainimpl.autoupdates.UpdaterWorker
import com.vargag99.autoplaylist.domainimpl.autoupdates.UpdaterWorker.Companion.INITIAL_DELAY_SECONDS
import timber.log.Timber
import java.util.concurrent.TimeUnit

class MediaScanUpdateScheduler(
    private val workManager: WorkManager,
    private val preferences: Preferences,
    private val mediaScanBroadcastEnabler: MediaScanBroadcastEnabler
) : UpdateScheduler, MediaScanReceiver.EventHandler, UpdaterWorker.Listener {
    override fun scheduleUpdatesAsNeeded() {
        Timber.d("scheduleUpdatesAsNeeded")
        if (preferences.enabled.get()) scheduleUpdates() else cancelUpdates()
    }

    private fun scheduleUpdates() {
        Timber.i("scheduleUpdates")
        mediaScanBroadcastEnabler.enable()
    }

    private fun cancelUpdates() {
        Timber.i("cancelUpdates")
        mediaScanBroadcastEnabler.disable()
    }

    override fun onMediaScannerFinished() {
        Timber.d("onMediaScannerFinished")
        val workRequest = OneTimeWorkRequest.Builder(UpdaterWorker::class.java)
            .setInitialDelay(INITIAL_DELAY_SECONDS, TimeUnit.SECONDS)
            .build()
        workManager.enqueueUniqueWork(UpdaterWorker.NAME, ExistingWorkPolicy.REPLACE, workRequest)
    }

    override fun onUpdateWorkerCompleted() = Unit
}
