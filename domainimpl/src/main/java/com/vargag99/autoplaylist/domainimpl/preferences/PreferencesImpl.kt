package com.vargag99.autoplaylist.domainimpl.preferences

import android.content.Context
import androidx.preference.PreferenceManager
import com.f2prateek.rx.preferences2.RxSharedPreferences
import com.vargag99.autoplaylist.domain.preferences.Preferences

class PreferencesImpl(context: Context) : Preferences {
    private val prefs = RxSharedPreferences.create(PreferenceManager.getDefaultSharedPreferences(context))

    override val enabled = prefs.getBoolean(ENABLED, true)

    override var version = prefs.getInteger(VERSION, 0)

    override var internalOnlyAlertShown = prefs.getBoolean(INTERNAL_ONLY_ALERT_SHOWN, false)

    companion object {
        const val ENABLED = "ENABLED"
        @Suppress("Unused") // Unused setting, not in this version.
        const val UNIQUE_NAMES = "UNIQUE_NAMES"
        @Suppress("Unused") // Unused setting, not in this version.
        const val ANALYTICS_ENABLED = "ANALYTICS_ENABLED"
        const val VERSION = "VERSION" // Detect upgrades.
        const val INTERNAL_ONLY_ALERT_SHOWN = "INTERNAL_ONLY_ALERT_SHOWN"
    }
}
