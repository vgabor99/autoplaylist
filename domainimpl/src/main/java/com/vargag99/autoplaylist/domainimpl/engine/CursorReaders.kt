package com.vargag99.autoplaylist.domainimpl.engine

import android.database.Cursor

class LongReader(private val cursor: Cursor, private val columnIndex: Int) {
    operator fun invoke(): Long = cursor.getLong(columnIndex)
}

class StringReader(private val cursor: Cursor, private val columnIndex: Int) {
    operator fun invoke(): String? = cursor.getString(columnIndex)
}

fun Cursor.longReader(columnName: String) = LongReader(this, getColumnIndexOrThrow(columnName))
fun Cursor.stringReader(columnName: String) = StringReader(this, getColumnIndexOrThrow(columnName))
