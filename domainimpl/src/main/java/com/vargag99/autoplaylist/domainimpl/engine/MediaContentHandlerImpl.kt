@file:Suppress("MaximumLineLength", "MaxLineLength")

package com.vargag99.autoplaylist.domainimpl.engine

import android.content.ContentResolver
import android.content.ContentUris
import android.content.ContentValues
import android.provider.MediaStore
import com.vargag99.autoplaylist.domain.analytics.Analytics
import com.vargag99.autoplaylist.domain.analytics.Events
import com.vargag99.autoplaylist.domain.analytics.Properties
import timber.log.Timber

@Suppress("TooManyFunctions")
class MediaContentHandlerImpl(
    private val contentResolver: ContentResolver,
    private val analytics: Analytics
) : MediaContentHandler {

    override fun countPlaylists(): Int {
        // Must use cursor.count, COUNT(*) AS COUNT is not working in Android 10
        return contentResolver.query(
            MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI,
            null,
            "${MediaStore.Audio.Playlists.NAME} LIKE '${Constants.PLAYLIST_PREFIX}%'",
            null,
            null)?.use { cursor ->
            cursor.count
        } ?: 0
    }

    override fun cleanupPlaylists(): Int {
        val count = contentResolver.delete(
            MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI,
            "${MediaStore.Audio.Playlists.NAME} LIKE '${Constants.PLAYLIST_PREFIX}%'",
            null)
        analytics.event(Events.CLEANUP, Properties.VALUE to count)
        Timber.i("Cleanup: deleted $count lists")
        return count
    }

    override fun updatePlaylists(folderFilter: List<String>?): Int {
        val allTracks = getTracks()
        val tracks = allTracks.filter(folderFilter.toFilter())
        val dirs = tracks.groupByDirs()
        Timber.d("Update: dirs ${dirs.size}, filterDirs ${folderFilter?.size}, allTracks ${allTracks.size}, tracks ${tracks.size}")
        logFilters(folderFilter)
        makeInitialPlayListNames(dirs)
        makeUniquePlayListNames(dirs)
        val badPlayLists = checkExistingPlayLists(dirs)
        val numDeleted = deletePlayLists(badPlayLists)
        val numAdded = makeNewPlayLists(dirs)
        val count = countPlayLists(dirs)
        analytics.event(Events.UPDATE,
            Properties.VALUE to count,
            Properties.FILTERED to (folderFilter != null)
        )
        Timber.i("Update: deleted $numDeleted, added $numAdded, have $count lists")
        return count
    }

    private fun List<String>?.toFilter(): (TrackInfo) -> Boolean =
        if (this == null) {
            { true }
        } else {
            { trackInfo -> this.any { trackInfo.dir.startsWith(it) } }
        }

    fun getTracks(): Set<TrackInfo> {
        val tracks = mutableSetOf<TrackInfo>()
        contentResolver.query(
            MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
            arrayOf(MediaStore.Audio.Media._ID, MediaStore.Audio.Media.DATA),
            "${MediaStore.Audio.Media.IS_MUSIC} != 0",
            null,
            null)?.use { cursor ->
            val id = cursor.longReader(MediaStore.Audio.Media._ID)
            val data = cursor.stringReader(MediaStore.Audio.Media.DATA)
            while (cursor.moveToNext()) {
                val info = TrackInfo.from(id(), data())
                if (info != null) tracks.add(info)
            }
        }
        return tracks
    }

    private fun getPlayListTracks(playListId: Long): List<TrackInfo> {
        val tracks = mutableListOf<TrackInfo>()
        contentResolver.query(
            MediaStore.Audio.Playlists.Members.getContentUri("external", playListId),
            arrayOf(MediaStore.Audio.Playlists.Members.AUDIO_ID, MediaStore.MediaColumns.DATA),
            null,
            null,
            MediaStore.Audio.Playlists.Members.PLAY_ORDER)?.use { cursor ->
            val trackId = cursor.longReader(MediaStore.Audio.Playlists.Members.AUDIO_ID)
            val data = cursor.stringReader(MediaStore.MediaColumns.DATA)
            while (cursor.moveToNext()) {
                val info = TrackInfo.from(trackId(), data())
                if (info != null) tracks.add(info)
            }
        }
        return tracks
    }

    private fun checkExistingPlayLists(dirs: Map<String, DirInfo>): List<Long> {
        // Iterate over existing playlists. If the playlist is good, note the playlist id in the dir map.
        // If the playlist is bad (does not match the tracks in the dir), add to the "bad" list, which is returned.
        // Bad playlists are:
        // - Empty playlist (it is useless);
        // - The playlist has tracks from multiple dirs;
        // - The tracks in the dir do not match the playlist.
        // - The playlist name does not match the folder name.
        val playListsToDelete = mutableListOf<Long>()
        contentResolver.query(
            MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI,
            arrayOf(MediaStore.Audio.Playlists._ID, MediaStore.Audio.Playlists.NAME),
            "${MediaStore.Audio.Playlists.NAME} LIKE '${Constants.PLAYLIST_PREFIX}%'",
            null,
            null)?.use { cursor ->
            val id = cursor.longReader(MediaStore.Audio.Playlists._ID)
            val name = cursor.stringReader(MediaStore.Audio.Playlists.NAME)
            while (cursor.moveToNext()) {
                val playList = PlayListInfo(id(), name())
                val matchingDir = dirs.findDirMatching(playList)
                if (matchingDir != null) {
                    matchingDir.playListId = playList.id // Keep this playlist.
                } else {
                    playListsToDelete.add(playList.id) // Delete this playlist.
                }
            }
        }
        return playListsToDelete
    }

    private fun Map<String, DirInfo>.findDirMatching(playlist: PlayListInfo): DirInfo? {
        val playListTracks = getPlayListTracks(playlist.id)
        val dir = playListTracks.getDir()
        if (dir != null) { // Playlist is not empty, and contains tracks from a single dir.
            return this[dir]?.takeIf { dirInfo ->
                dirInfo.playListName == playlist.name && // Playlist name is OK (matches the dir)
                    playListTracks matches dirInfo.tracks // Playlist matches tracks in dir
            }
        }
        return null
    }

    private fun deletePlayLists(playLists: List<Long>): Int {
        var numDeleted = 0
        if (playLists.isNotEmpty()) {
            // Delete the wrong playlists now.
            // We can't bulk delete because of failures on Android 10.
            // The ID must be part of the URI for deletion.
            for (id in playLists) {
                val uri = ContentUris.withAppendedId(MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI, id)
                numDeleted += contentResolver.delete(uri, null, null)
            }
            if (numDeleted != playLists.size) {
                Timber.w("Deleted $numDeleted lists instead of ${playLists.size}")
            }
        }
        return numDeleted
    }

    private fun makeNewPlayLists(dirs: Map<String, DirInfo>): Int {
        // For each dir that has no playlist, add one now.
        val now = System.currentTimeMillis()
        return dirs.values.filter { it.playListId == 0L }.count { dir ->
            makeNewPlaylist(dir, now)
        }
    }

    private fun makeNewPlaylist(dir: DirInfo, timestamp: Long): Boolean {
        val values = ContentValues().apply {
            put(MediaStore.Audio.Playlists.NAME, dir.playListName)
            put(MediaStore.Audio.Playlists.DATE_ADDED, timestamp)
            put(MediaStore.Audio.Playlists.DATE_MODIFIED, timestamp)
        }
        val uri = contentResolver.insert(MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI, values)
        if (uri != null) {
            dir.playListId = ContentUris.parseId(uri) // Store playlist id.
            // Add tracks to track list.
            val values = arrayOfNulls<ContentValues>(dir.tracks.size)
            for (i in dir.tracks.indices) {
                values[i] = ContentValues().apply {
                    put(MediaStore.Audio.Playlists.Members.PLAY_ORDER, i + 1)
                    put(MediaStore.Audio.Playlists.Members.AUDIO_ID, dir.tracks[i].id)
                }
            }
            contentResolver.bulkInsert(uri, values)
            return true
        } else {
            Timber.w("Failed to create list '${dir.playListName}' for '${dir.fn}'")
            return false
        }
    }

    private fun countPlayLists(dirs: Map<String, DirInfo>): Int =
        dirs.values.count { it.playListId != 0L }

    private fun logFilters(folderFilter: List<String>?) {
        Timber.v("Filters")
        if (folderFilter == null) {
            Timber.v("  (empty)")
        } else {
            folderFilter.forEach { Timber.v("  '$it'") }
        }
    }
}
