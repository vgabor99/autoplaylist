package com.vargag99.autoplaylist.domainimpl.engine

interface MediaContentHandler {
    fun countPlaylists(): Int
    fun cleanupPlaylists(): Int
    fun updatePlaylists(folderFilter: List<String>?): Int
}
