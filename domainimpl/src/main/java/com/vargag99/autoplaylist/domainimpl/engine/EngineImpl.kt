package com.vargag99.autoplaylist.domainimpl.engine

import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.vargag99.autoplaylist.domain.engine.Engine
import com.vargag99.autoplaylist.domain.engine.MediaScannerObserver
import com.vargag99.autoplaylist.domain.engine.MediaScannerRunningException
import com.vargag99.autoplaylist.domain.preferences.Preferences
import com.vargag99.autoplaylist.domain.schedulers.Schedulers
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.processors.BehaviorProcessor
import io.reactivex.processors.PublishProcessor
import timber.log.Timber
import java.util.concurrent.atomic.AtomicBoolean

class EngineImpl(
    private val mediaContentHandler: MediaContentHandler,
    private val mediaScannerObserver: MediaScannerObserver,
    private val preferences: Preferences,
    private val filterProvider: FilterProvider,
    schedulers: Schedulers
) : Engine {
    private val dirty = AtomicBoolean(false)
    private val scheduler = schedulers.single
    private val _numLists = BehaviorProcessor.create<Int>()
    private val _state = BehaviorProcessor.createDefault(Engine.State.IDLE)
    private val _errors = PublishProcessor.create<Throwable>()
    override val numLists: Flowable<Int>
        get() = _numLists.share()
    override val state: Flowable<Engine.State>
        get() = _state.share()
    override val errors: Flowable<Throwable>
        get() = _errors.share()

    override fun count(): Completable = Completable.fromCallable {
        Timber.d("count")
        _state.offer(Engine.State.COUNTING)
        _numLists.offer(mediaContentHandler.countPlaylists())
        _state.offer(Engine.State.IDLE)
    }
        .subscribeOn(scheduler)
        .doOnError(::emitError)

    override fun update(): Completable = Completable.fromCallable {
        Timber.d("update")
        if (dirty.compareAndSet(true, false)) {
            try {
                _state.offer(Engine.State.UPDATING)
                when {
                    !preferences.enabled.get() -> doCleanup()
                    mediaScannerObserver.isMediaScannerRunning -> throw MediaScannerRunningException()
                    else -> doUpdate()
                }
            } finally {
                _state.offer(Engine.State.IDLE)
            }
        } else {
            Timber.d("update skipped: not dirty")
        }
    }
        .subscribeOn(scheduler)
        .doOnError(::emitError)
        .doOnSubscribe { dirty.set(true) }

    private fun doCleanup() {
        Timber.d("doCleanup")
        mediaContentHandler.cleanupPlaylists()
        _numLists.offer(0)
    }

    private fun doUpdate() {
        Timber.d("doUpdate")
        val folderFilter = filterProvider.getPaths().takeIf { it.isNotEmpty() }
        _numLists.offer(mediaContentHandler.updatePlaylists(folderFilter))
    }

    private fun emitError(throwable: Throwable) {
        Timber.e(throwable, "emitError")
        FirebaseCrashlytics.getInstance().recordException(throwable)
        _errors.offer(throwable)
    }
}
