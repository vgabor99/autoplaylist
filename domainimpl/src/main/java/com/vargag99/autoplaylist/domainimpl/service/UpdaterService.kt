package com.vargag99.autoplaylist.domainimpl.service

import android.content.Intent
import com.vargag99.autoplaylist.domain.engine.Engine
import com.vargag99.autoplaylist.domainimpl.util.RxIntentService
import io.reactivex.Completable
import org.koin.android.ext.android.inject
import timber.log.Timber

class UpdaterService : RxIntentService() {
    private val engine: Engine by inject()

    override fun onCreate() {
        Timber.d("onCreate")
        super.onCreate()
    }

    override fun onDestroy() {
        Timber.d("onDestroy")
        super.onDestroy()
    }

    override fun createOperation(intent: Intent?): Completable? {
        Timber.d("createOperation $intent")
        return when (intent?.action) {
            ACTION_COUNT -> engine.count()
            ACTION_UPDATE -> engine.update()
            else -> null
        }
    }

    companion object {
        const val ACTION_UPDATE = "ACTION_UPDATE"
        const val ACTION_COUNT = "ACTION_COUNT"
    }
}
