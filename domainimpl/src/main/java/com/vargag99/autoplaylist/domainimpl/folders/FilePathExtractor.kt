package com.vargag99.autoplaylist.domainimpl.folders

import android.net.Uri
import com.vargag99.autoplaylist.domain.folders.FilePath

interface FilePathExtractor {
    fun getFilePath(uri: Uri): FilePath?
}
