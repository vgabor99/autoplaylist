package com.vargag99.autoplaylist.domainimpl.service

import android.content.Context
import android.content.Intent
import com.vargag99.autoplaylist.domain.service.ServiceCommands
import timber.log.Timber

class ServiceCommandsImpl(private val context: Context) : ServiceCommands {

    override fun count() = doAction(UpdaterService.ACTION_COUNT)

    override fun update() = doAction(UpdaterService.ACTION_UPDATE)

    private fun doAction(action: String) {
        Timber.d("doAction $action")
        val intent = Intent(context, UpdaterService::class.java)
        intent.action = action
        context.startService(intent)
    }
}
