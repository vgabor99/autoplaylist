package com.vargag99.autoplaylist.domainimpl.folders

import android.net.Uri
import android.os.Build
import com.vargag99.autoplaylist.domain.folders.Folder
import com.vargag99.autoplaylist.domain.folders.FolderRepo
import com.vargag99.autoplaylist.domain.folders.TreeUri
import com.vargag99.autoplaylist.domain.folders.UnsupportedTreeUriException
import com.vargag99.autoplaylist.domain.folders.UnsupportedVolumeException
import com.vargag99.autoplaylist.domain.folders.Volume
import com.vargag99.autoplaylist.domain.folders.fullPath
import com.vargag99.autoplaylist.domainimpl.engine.FilterProvider
import io.reactivex.Flowable

class FolderRepoImpl(
    private val treeUriRepo: TreeUriRepo,
    private val filePathExtractor: FilePathExtractor,
    private val volumeManager: VolumeManager
) : FolderRepo, FilterProvider {
    override fun getFolders(): List<Folder> =
        treeUriRepo.getTreeUris().mapNotNull(::toFolder)

    override fun getFoldersFlowable(): Flowable<List<Folder>> =
        treeUriRepo.getTreeUrisFlowable().map { it.mapNotNull(::toFolder) }

    override fun deleteFolder(folder: Folder) =
        treeUriRepo.deleteTreeUri(folder.treeUri)

    override fun addFolder(uri: Uri) {
        val path = filePathExtractor.getFilePath(uri) ?: throw UnsupportedTreeUriException(uri)
        if (!isSupportedVolume(path.volume)) {
            throw UnsupportedVolumeException(uri)
        }
        treeUriRepo.addTreeUri(uri)
    }

    override fun getPaths(): List<String> {
        val goodVolumes = getSupportedVolumes()
        val folders = getFolders().filter { goodVolumes.contains(it.path.volume) }
        return folders.map { it.path.fullPath }.takeIf { it.isNotEmpty() }
            ?: goodVolumes.map { it.path }
    }

    private fun toFolder(treeUri: TreeUri): Folder? =
        filePathExtractor.getFilePath(treeUri.treeUri)?.let { Folder(treeUri, it) }

    // Overcome Android Q bug by limiting operation to internal storage.
    // https://issuetracker.google.com/issues/147619577
    private fun getSupportedVolumes() =
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) volumeManager.availableVolumes()
        else volumeManager.availableVolumes().filter { it.isPrimary }

    private fun isSupportedVolume(volume: Volume) =
        getSupportedVolumes().contains(volume)
}
