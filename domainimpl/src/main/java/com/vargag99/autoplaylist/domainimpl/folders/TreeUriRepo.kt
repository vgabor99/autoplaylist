package com.vargag99.autoplaylist.domainimpl.folders

import android.net.Uri
import com.vargag99.autoplaylist.domain.folders.TreeUri
import io.reactivex.Flowable

interface TreeUriRepo {
    fun getTreeUris(): List<TreeUri>
    fun getTreeUrisFlowable(): Flowable<List<TreeUri>>
    fun deleteTreeUri(treeUri: TreeUri)
    fun addTreeUri(uri: Uri)
}
