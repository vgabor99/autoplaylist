package com.vargag99.autoplaylist.domainimpl.permission

import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat
import com.vargag99.autoplaylist.domain.permission.PermissionHandler

class PermissionHandlerImpl(private val context: Context) : PermissionHandler {
    override fun missingPermissions(): List<String> = listOf(READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE)
        .filter { ContextCompat.checkSelfPermission(context, it) != PackageManager.PERMISSION_GRANTED }
}
