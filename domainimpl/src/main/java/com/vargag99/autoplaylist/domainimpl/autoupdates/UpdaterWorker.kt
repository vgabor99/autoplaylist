package com.vargag99.autoplaylist.domainimpl.autoupdates

import android.content.Context
import androidx.work.RxWorker
import androidx.work.WorkerParameters
import com.vargag99.autoplaylist.domain.analytics.Analytics
import com.vargag99.autoplaylist.domain.analytics.Events
import com.vargag99.autoplaylist.domain.engine.Engine
import com.vargag99.autoplaylist.domain.engine.MediaScannerRunningException
import com.vargag99.autoplaylist.domain.schedulers.Schedulers
import io.reactivex.Single
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import timber.log.Timber

class UpdaterWorker(
    context: Context,
    workerParams: WorkerParameters
) : RxWorker(context, workerParams), KoinComponent {

    interface Listener {
        fun onUpdateWorkerCompleted()
    }

    private val schedulers: Schedulers by inject()
    private val engine: Engine by inject()
    private val updateWorkerObserver: Listener by inject()
    private val analytics: Analytics by inject()

    override fun createWork(): Single<Result> {
        Timber.d("createWork")
        return engine.update()
            .observeOn(schedulers.main)
            .doOnSubscribe {
                analytics.event(Events.AUTO_UPDATE)
                Timber.i("running")
            }
            .onErrorComplete { it is MediaScannerRunningException }
            .doOnComplete { updateWorkerObserver.onUpdateWorkerCompleted() }
            .toSingle(Result::success)
            .doOnError(Timber::e)
            .onErrorReturnItem(Result.retry())
    }

    override fun onStopped() {
        Timber.d("onStopped")
        super.onStopped()
        updateWorkerObserver.onUpdateWorkerCompleted()
    }

    companion object {
        const val NAME = "Update"
        const val INITIAL_DELAY_SECONDS = 2L
    }
}
