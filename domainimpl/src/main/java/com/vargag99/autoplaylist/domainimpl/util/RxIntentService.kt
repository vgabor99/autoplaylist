package com.vargag99.autoplaylist.domainimpl.util

import android.app.Service
import android.content.Intent
import android.os.IBinder
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import kotlin.properties.Delegates

abstract class RxIntentService : Service() {
    @Suppress("WeakerAccess")
    protected var intentRedelivery: Boolean = false
    private var disposables = CompositeDisposable()
    private var count: Int by Delegates.observable(0) { _, _, newValue ->
        if (newValue == 0) stopSelf()
    }

    override fun onDestroy() {
        super.onDestroy()
        disposables.dispose()
    }

    final override fun onBind(intent: Intent?): IBinder? = null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        createOperation(intent)?.let { operation ->
            count++
            operation
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally { count-- }
                .subscribeBy(
                    onError = { Timber.e(it, "Operation failed: $intent") },
                    onComplete = {}
                )
                .addTo(disposables)
        }
        return if (intentRedelivery) START_REDELIVER_INTENT else START_NOT_STICKY
    }

    protected abstract fun createOperation(intent: Intent?): Completable?
}
