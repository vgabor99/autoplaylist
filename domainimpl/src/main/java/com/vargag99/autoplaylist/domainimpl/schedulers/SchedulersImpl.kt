package com.vargag99.autoplaylist.domainimpl.schedulers

import com.vargag99.autoplaylist.domain.schedulers.Schedulers
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers as RxSchedulers

class SchedulersImpl : Schedulers {
    override val io: Scheduler
        get() = RxSchedulers.io()
    override val main: Scheduler
        get() = AndroidSchedulers.mainThread()
    override val single: Scheduler
        get() = RxSchedulers.single()
}
