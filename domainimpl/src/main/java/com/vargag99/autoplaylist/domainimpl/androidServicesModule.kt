package com.vargag99.autoplaylist.domainimpl

import android.content.Context
import android.os.storage.StorageManager
import androidx.core.content.getSystemService
import androidx.work.WorkManager
import org.koin.dsl.module

val androidServicesModule = module {
    factory { get<Context>().contentResolver }
    factory { get<Context>().getSystemService<StorageManager>() }
    factory { WorkManager.getInstance(get()) }
}
