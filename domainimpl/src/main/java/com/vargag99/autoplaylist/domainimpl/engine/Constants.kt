package com.vargag99.autoplaylist.domainimpl.engine

object Constants {
    const val PLAYLIST_PREFIX = "@ "
    const val PLAYLIST_DEPTH_SEPARATOR = "/"
}
