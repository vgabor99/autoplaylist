package com.vargag99.autoplaylist.domainimpl.engine

import timber.log.Timber
import java.io.File
import java.text.Normalizer
import java.util.Comparator
import java.util.Locale

fun makeInitialPlayListNames(dirs: Map<String, DirInfo>) {
    // Make a playlist name for each of the dirs.
    // Initial playlist names - matches the dir name.
    for (dir in dirs.values) {
        dir.playListGenDepth = 1
        dir.maxedOut = false
        makeLengthBasedName(dir)
    }
    logDirs(dirs.values, "Initial names")
}

private const val MAX_CONFLICT_ROUNDS = 20
fun makeUniquePlayListNames(dirs: Map<String, DirInfo>) { // Check conflicts and fix them.
    var conflict: Boolean
    var conflictRounds = 0
    do {
        conflictRounds++
        check(conflictRounds < MAX_CONFLICT_ROUNDS) {
            // Safety code - better to fail that go in an infinite loop :(
            "Bailout at $conflictRounds conflict rounds"
        }
        // Build conflict map (playlist name -> dirs that use the playlist name).
        val conflictMap = buildConflictMap(dirs)
        // Fix the conflicts by increasing the name generation depth of the conflicting dirs.
        conflict = false
        for ((key, value) in conflictMap) {
            if (value.size > 1) {
                // This playlist name is conflicting, make a resolve round.
                // Resolving is done in rounds. We have a set that is currently conflicting on
                // a name, so we generate a new names for those playlists. However, we cannot be
                // sure that the resolution does not bring in other dirs to the conflict. So if
                // we have one single conflict anywhere, we have to restart checking as a whole.
                conflict = true
                logDirs(value, "Conflict on '$key")
                resolveRound(value)
            }
        }
    } while (conflict)
    if (conflictRounds > 0) {
        logDirs(dirs.values, "Conflicts resolved, $conflictRounds rounds")
    } else {
        Timber.v("No conflicts")
    }
}

private fun buildConflictMap(dirs: Map<String, DirInfo>): Map<String, Set<DirInfo>> {
    // Build conflict map (reverse map: playlist name to DirInfo set that uses that playlist name).
    val conflictMap = mutableMapOf<String, MutableSet<DirInfo>>()
    for (dir in dirs.values) {
        val playListName = normalize(dir.playListName)
        conflictMap.getOrPut(playListName, { mutableSetOf() }).add(dir)
    }
    return conflictMap
}

private fun resolveRound(conflictingDirs: Set<DirInfo>) {
    // The set contains the dirs that conflict on a single playlist name.
    // Make the next round of resolving.
    // The original way of resolving is making the name longer. At the extreme,
    // this would end up a playlist name that contains the complete path to he
    // dir, which is always unique, so we could never "max out".
    //
    // But 4.3 brought in the collated name comparison, so two paths may
    // produce the same playlist name and we can "max out" the length. For
    // maxed-out conflicts, we need a different conflict resolution (postfix).
    // Check what way of conflict resolution can work - are we maxed out yet?
    var maxedOut = 0
    for (dir in conflictingDirs) {
        if (dir.maxedOut) {
            maxedOut++
        }
    }
    // Now resolve the conflict.
    if (maxedOut > 1) {
        // Maxed out already. We have conflicts with at least two maxed-out dirs,
        // increasing the name generation depth no longer works. Max out all in
        // this conflict set.
        for (dir in conflictingDirs) {
            dir.maxedOut = true
        }
        // Resolve conflict the maxed-out way (add numeric postfix).
        var postfix = 0
        for (dir in conflictingDirs.sortedWith(Comparator { left, right -> left.fn.compareTo(right.fn) })) {
            postfix++
            makePostfixedName(dir, postfix)
        }
    } else {
        // Resolve conflict the normal way (make longer names).
        // This also sets the maxed-out flag, if applicable.
        for (dir in conflictingDirs) {
            dir.playListGenDepth++
            makeLengthBasedName(dir)
        }
    }
}

private fun makePostfixedName(dir: DirInfo, postfix: Int) {
    // We use the dir name plus a numeric postfix.
    // "sdcard/foo/Music" -> "@ Music (1)"
    // "sdcard/foo/music" -> "@ music (2)"
    val names = dir.fn.split(File.separator).toTypedArray()
    val name = names.last()
    dir.playListName = "${Constants.PLAYLIST_PREFIX}$name($postfix)"
}

private fun makeLengthBasedName(dir: DirInfo) {
    // We build the track list name from the dir name, from 'depth' number of names.
    // "/sdcard/foo/bar", 1 -> "@ bar"
    // "/sdcard/foo/bar", 2 -> "@ foo/bar"
    val names = dir.fn.split(File.separator).toTypedArray()
    var first = names.size - dir.playListGenDepth
    if (first <= 1) {
        // Maxed out - cannot increase length any longer.
        // We start from 1, not 0 - the 0th part is the "" before the root '/'.
        // It is not pretty and does not help resolving, so we might as well ignore it.
        // --> The generated playlist names never start with '/'.
        dir.maxedOut = true
        first = 1
    }
    dir.playListName = Constants.PLAYLIST_PREFIX + names.slice(first until names.size)
        .joinToString(separator = Constants.PLAYLIST_DEPTH_SEPARATOR)
}

private fun normalize(name: String?): String {
    // Collate playlist name for uniqueness comparison.
    return Normalizer
        .normalize(name, Normalizer.Form.NFD)
        .replace("\\p{InCombiningDiacriticalMarks}+".toRegex(), "")
        .toLowerCase(Locale.US)
}

private fun logDirs(dirs: Collection<DirInfo>, title: String) {
    Timber.v(title)
    for (dir in dirs) {
        Timber.v("  ${dir.compactLog()}")
    }
}

private fun DirInfo.compactLog(): String {
    val maxedOut = if (maxedOut) "!" else ""
    return "($playListGenDepth$maxedOut) '$fn' -> '$playListName'"
}
