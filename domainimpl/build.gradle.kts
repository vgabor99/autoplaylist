plugins {
    id("autoplaylist-library")
}

dependencies {
    implementation(project(":domain"))
    implementation(Deps.Koin.android)
    implementation(Deps.timber)
    implementation(Deps.Androidx.preferenceKtx)
    implementation(Deps.Androidx.coreKtx)
    implementation(platform(Deps.Firebase.bom))
    implementation(Deps.Firebase.core)
    implementation(Deps.Firebase.crashlytics)
    implementation(Deps.Androidx.Work.runtime)
    implementation(Deps.Androidx.Work.rxjava2)
}
