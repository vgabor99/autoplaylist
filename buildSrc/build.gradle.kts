plugins {
    `kotlin-dsl`
}

gradlePlugin {
    plugins {
        create("autoplaylistAppPlugin") {
            id = "autoplaylist-app"
            implementationClass = "AutoPlaylistAppPlugin"
        }
        create("autoplaylistLibraryPlugin") {
            id = "autoplaylist-library"
            implementationClass = "AutoPlaylistLibraryPlugin"
        }
    }
}

repositories {
    google()
    jcenter()
    mavenCentral()
    maven(url = "https://plugins.gradle.org/m2/")
}

dependencies {
    implementation("com.android.tools.build:gradle:4.1.0")
    implementation("org.jetbrains.kotlin:kotlin-gradle-plugin:1.4.10")
    implementation("com.google.gms:google-services:4.3.4")
    implementation("com.google.firebase:firebase-crashlytics-gradle:2.3.0")
    compileOnly(gradleApi())
    compileOnly(localGroovy())
}
