import com.android.build.gradle.api.AndroidBasePlugin
import org.gradle.api.Project
import org.gradle.kotlin.dsl.dependencies

object Versions {
    const val compileSdkVersion = 29
    const val buildToolsVersion = "29.0.3"
    const val minSdkVersion = 21
    const val targetSdkVersion = 29
}

object Deps {

    object Kotlin {
        const val version = "1.4.10"
        const val stdlibJdk7 = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:$version"
    }

    object RxJava {
        const val rxandroid = "io.reactivex.rxjava2:rxandroid:2.1.1"
        const val rxjava = "io.reactivex.rxjava2:rxjava:2.2.20"
        const val rxkotlin = "io.reactivex.rxjava2:rxkotlin:2.4.0"
    }

    object Koin {
        const val version = "2.2.0"
        const val android = "org.koin:koin-android:$version"
        const val androidxScope = "org.koin:koin-androidx-scope:$version"
        const val androidxViewmodel = "org.koin:koin-androidx-viewmodel:$version"
    }

    const val timber = "com.jakewharton.timber:timber:4.7.1"
    const val rxPreferences = "com.f2prateek.rx.preferences2:rx-preferences:2.0.1"

    object Androidx {
        const val legacySupportV4 = "androidx.legacy:legacy-support-v4:1.0.0"
        const val coreKtx = "androidx.core:core-ktx:1.3.2"
        const val preferenceKtx = "androidx.preference:preference-ktx:1.1.1"
        const val fragmentKtx = "androidx.fragment:fragment-ktx:1.2.5"
        const val constraintlayout = "androidx.constraintlayout:constraintlayout:2.0.4"

        object Work {
            const val version = "2.4.0"
            const val runtime = "androidx.work:work-runtime:$version"
            const val rxjava2 = "androidx.work:work-rxjava2:$version"
        }
    }

    object Firebase {
        const val bom = "com.google.firebase:firebase-bom:26.2.0"
        const val core = "com.google.firebase:firebase-core"
        const val crashlytics = "com.google.firebase:firebase-crashlytics"
    }

    object Lifecycle {
        const val version = "2.2.0"
        const val extensions = "androidx.lifecycle:lifecycle-extensions:$version"
        const val viewmodelKtx = "androidx.lifecycle:lifecycle-viewmodel-ktx:$version"
        const val livedataKtx = "androidx.lifecycle:lifecycle-livedata-ktx:$version"
        const val reactivestreamsKtx = "androidx.lifecycle:lifecycle-reactivestreams-ktx:$version"
    }

    const val switcherx = "com.bitvale:switcher:1.1.0"

    object BindingCollectionAdapter {
        const val version = "4.0.0"
        const val adapter = "me.tatarka.bindingcollectionadapter2:bindingcollectionadapter:$version"
        const val recyclerview = "me.tatarka.bindingcollectionadapter2:bindingcollectionadapter-recyclerview:$version"
    }

    const val material = "com.google.android.material:material:1.2.1"

    object Detekt {
        const val version = "1.15.0"
        const val formatting = "io.gitlab.arturbosch.detekt:detekt-formatting:$version"
    }

    object Test {
        const val junit = "androidx.test.ext:junit:1.1.2"
        const val runner = "androidx.test:runner:1.3.0"
        const val rules = "androidx.test:rules:1.3.0"
    }

    object Mockk {
        const val version = "1.10.2"
        const val mockk = "io.mockk:mockk:$version"
        const val mockkAndroid = "io.mockk:mockk-android:$version"
    }

    object GradleVersions {
        const val version = "0.36.0"
        const val versions = "com.github.ben-manes.versions:$version"
    }
}

internal fun Project.configureDependencies() = dependencies {
    add("implementation", Deps.Kotlin.stdlibJdk7)
    add("implementation", Deps.RxJava.rxjava)
    add("implementation", Deps.RxJava.rxandroid)
    add("implementation", Deps.RxJava.rxkotlin)

    if (project.plugins.hasPlugin("io.gitlab.arturbosch.detekt")) {
        add("detektPlugins", Deps.Detekt.formatting)
    }

    add("testImplementation", Deps.Test.junit)
    add("testImplementation", Deps.Mockk.mockk)

    if (project.containsAndroidPlugin()) {
        add("androidTestImplementation", Deps.Test.junit)
        add("androidTestImplementation", Deps.Test.runner)
        add("androidTestImplementation", Deps.Mockk.mockkAndroid)
    }
}

internal fun Project.containsAndroidPlugin(): Boolean {
    return project.plugins.toList().any { it is AndroidBasePlugin }
}
