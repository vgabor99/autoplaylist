import org.gradle.api.Plugin
import org.gradle.api.Project

open class AutoPlaylistAppPlugin : Plugin<Project> {
    override fun apply(project: Project) {
        project.plugins.apply("com.android.application")
        project.plugins.apply("kotlin-android")
        project.plugins.apply("kotlin-kapt")
        project.plugins.apply("kotlin-android-extensions")
        project.plugins.apply("io.gitlab.arturbosch.detekt")
        project.configureAndroidApp()
        project.configureDependencies()
    }
}
