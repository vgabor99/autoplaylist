## Auto Playlist (discontinued)

Auto Playlist is a "play by folder" extension that works with any media player.

## Motivation

Before completely switching to streaming media consumption, I used to organize my music into folders. Not all phones and media players supported playing by folder. MediaStore playlists were part of the platform hence universally supported. Auto Playlist is built on the idea of getting universal support for folders by presenting them as playlists.

## Features

Auto Playlist is a background service that automatically creates a playlist for each music folder, and automatically keeps the lists in sync with the file system changes. It operates _solely_ on the MediaDb (no files are scanned): music files are accessed and observed via the MediaStore, and the resulting playlists are written to the MediaStore too.

Being a background service, the app has a minimal UI, and works "hands-free" with default configuration.

![](publishing/en/1.png)

Upon user request, a folder selection was added, to limit the playlist generation to selected folders.

![](publishing/en/2.png)

## Discontinuation

Playlists, the core idea powering the app, was removed from MediaStore in API 31.