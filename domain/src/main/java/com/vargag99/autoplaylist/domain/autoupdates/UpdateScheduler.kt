package com.vargag99.autoplaylist.domain.autoupdates

interface UpdateScheduler {
    fun scheduleUpdatesAsNeeded()
}
