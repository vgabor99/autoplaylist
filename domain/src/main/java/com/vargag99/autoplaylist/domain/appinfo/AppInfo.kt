package com.vargag99.autoplaylist.domain.appinfo

interface AppInfo {
    val appLabel: String
    val appVersionName: String
    val appVersionCode: Int
}
