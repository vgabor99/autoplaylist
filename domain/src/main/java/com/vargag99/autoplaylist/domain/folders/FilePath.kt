package com.vargag99.autoplaylist.domain.folders

data class FilePath(val volume: Volume, val path: String)

val FilePath.fullPath: String
    get() = "${volume.path}/$path"
