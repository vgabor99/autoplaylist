package com.vargag99.autoplaylist.domain.permission

interface PermissionHandler {
    fun missingPermissions(): List<String>
}
