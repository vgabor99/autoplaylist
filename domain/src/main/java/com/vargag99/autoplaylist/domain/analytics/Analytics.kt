package com.vargag99.autoplaylist.domain.analytics

import android.app.Activity

interface Analytics {
    fun screen(activity: Activity, name: String)
    fun event(event: String, vararg data: Pair<String, Any>)
}

@Suppress("SpreadOperator")
fun Analytics.click(label: String, vararg data: Pair<String, Any>) =
    event(Events.CLICK, Properties.LABEL to label, *data)

@Suppress("SpreadOperator")
fun Analytics.alert(label: String, vararg data: Pair<String, Any>) =
    event(Events.ALERT, Properties.LABEL to label, *data)

object Events {
    const val CLICK = "click"
    const val ALERT = "alert"
    const val PERMISSION_GRANT = "permission_grant"
    const val PERMISSION_DENY = "permission_deny"
    const val CLEANUP = "cleanup"
    const val UPDATE = "update"
    const val AUTO_UPDATE = "auto_update"
    const val DIRECTORY_PICK = "directory_pick"
    const val FIRST_RUN = "first_run"
    const val VERSION_CHANGED_RUN = "app_changed_run"
}

object Properties {
    const val LABEL = "label"
    const val VALUE = "value"
    const val MESSAGE = "message"
    const val FILTERED = "filtered"
}
