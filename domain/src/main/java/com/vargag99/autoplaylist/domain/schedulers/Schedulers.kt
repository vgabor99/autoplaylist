package com.vargag99.autoplaylist.domain.schedulers

import io.reactivex.Scheduler

interface Schedulers {
    val io: Scheduler
    val main: Scheduler
    val single: Scheduler
}
