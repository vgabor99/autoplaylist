package com.vargag99.autoplaylist.domain.folders

import android.net.Uri
import io.reactivex.Flowable

interface FolderRepo {
    fun getFolders(): List<Folder>
    fun getFoldersFlowable(): Flowable<List<Folder>>
    fun deleteFolder(folder: Folder)
    fun addFolder(uri: Uri)
}
