package com.vargag99.autoplaylist.domain.folders

import android.net.Uri

// Thrown when the selected tree has no file (not directory-backed). E.g. Recents, remote providers.
class UnsupportedTreeUriException(@Suppress("Unused") val treeUri: Uri) : Exception()
