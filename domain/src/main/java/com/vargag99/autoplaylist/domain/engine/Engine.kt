package com.vargag99.autoplaylist.domain.engine

import io.reactivex.Completable
import io.reactivex.Flowable

interface Engine {
    enum class State { IDLE, COUNTING, UPDATING }

    val numLists: Flowable<Int>
    val state: Flowable<State>
    val errors: Flowable<Throwable>

    fun count(): Completable
    fun update(): Completable
}
