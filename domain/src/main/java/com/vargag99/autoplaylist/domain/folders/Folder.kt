package com.vargag99.autoplaylist.domain.folders

data class Folder(val treeUri: TreeUri, val path: FilePath)
