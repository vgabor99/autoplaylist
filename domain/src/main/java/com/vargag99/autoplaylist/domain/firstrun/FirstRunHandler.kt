package com.vargag99.autoplaylist.domain.firstrun

interface FirstRunHandler {
    fun isFirstRunDone(): Boolean
    fun isAppVersionChanged(): Boolean
    fun triggerFirstRun(): Boolean
}
