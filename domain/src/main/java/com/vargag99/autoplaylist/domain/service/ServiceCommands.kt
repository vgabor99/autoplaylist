package com.vargag99.autoplaylist.domain.service

interface ServiceCommands {
    fun count()
    fun update()
}
