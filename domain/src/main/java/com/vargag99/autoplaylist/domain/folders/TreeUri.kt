package com.vargag99.autoplaylist.domain.folders

import android.net.Uri

data class TreeUri(val treeUri: Uri, val file: CachingDocumentFile)
