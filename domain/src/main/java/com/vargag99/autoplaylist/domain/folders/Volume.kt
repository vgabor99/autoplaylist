package com.vargag99.autoplaylist.domain.folders

data class Volume(
    val uuid: String?,
    val path: String,
    val isPrimary: Boolean,
    val description: String?
)
