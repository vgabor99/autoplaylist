package com.vargag99.autoplaylist.domain.engine

class MediaScannerRunningException : IllegalStateException("Media Scanner is running")
