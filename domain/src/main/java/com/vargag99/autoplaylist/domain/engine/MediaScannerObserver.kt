package com.vargag99.autoplaylist.domain.engine

import io.reactivex.Flowable

interface MediaScannerObserver {
    val isMediaScannerRunning: Boolean
    val isMediaScannerRunningFlowable: Flowable<Boolean>
}
