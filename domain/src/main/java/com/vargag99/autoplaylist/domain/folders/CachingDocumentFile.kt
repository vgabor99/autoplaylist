package com.vargag99.autoplaylist.domain.folders

import androidx.documentfile.provider.DocumentFile

data class CachingDocumentFile(private val documentFile: DocumentFile) {
    val name: String? by lazy { documentFile.name }
    val uri get() = documentFile.uri
}

fun DocumentFile.toCaching() = CachingDocumentFile(this)
