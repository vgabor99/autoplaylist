package com.vargag99.autoplaylist.domain.folders

import android.net.Uri

// Android Q bug: cannot make playlists if the music is on the SD card. So we have to reject it. :(
// https://issuetracker.google.com/issues/147619577
class UnsupportedVolumeException(@Suppress("Unused") val treeUri: Uri) : Exception()
