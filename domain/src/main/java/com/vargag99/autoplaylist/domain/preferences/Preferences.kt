package com.vargag99.autoplaylist.domain.preferences

import com.f2prateek.rx.preferences2.Preference

interface Preferences {
    val enabled: Preference<Boolean>
    var version: Preference<Int>
    var internalOnlyAlertShown: Preference<Boolean>
}
