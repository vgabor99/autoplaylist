plugins {
    id("autoplaylist-library")
}

dependencies {
    api(Deps.rxPreferences)
    api(Deps.Androidx.legacySupportV4)
}
