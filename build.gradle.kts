plugins {
    id("com.github.ben-manes.versions") version Deps.GradleVersions.version
    id("io.gitlab.arturbosch.detekt") version Deps.Detekt.version
}

detekt {
    toolVersion = Deps.Detekt.version
    failFast = true
    buildUponDefaultConfig = true
    input = files("domain/src", "domainimpl/src", "app/src")
    config = files("config/detekt/detekt.yml")

    reports {
        html.enabled = true
        xml.enabled = true
        txt.enabled = true
    }
}

buildscript {
    repositories {
        google()
        jcenter()
    }
    dependencies {
        classpath("com.android.tools.build:gradle:4.1.0")
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:${Deps.Kotlin.version}")
        classpath("com.google.gms:google-services:4.3.4")
        classpath("com.google.firebase:firebase-crashlytics-gradle:2.3.0")
    }
}

allprojects {
    repositories {
        google()
        jcenter()
        mavenCentral()
    }
}

tasks.register("clean", Delete::class) {
    delete(rootProject.buildDir)
}
